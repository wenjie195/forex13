<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/timezone.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/BuySell.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Current Trade | De Xin Guo Ji 德鑫国际" />
    <title>Current Trade | De Xin Guo Ji 德鑫国际</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
    <?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2 three-btn-div-padding2">
    <!-- <h1 class="menu-distance h1-title white-text text-center"><?php //echo _SIDEBAR_CURRENT_TRADE ?></h1> -->
    <h1 class="menu-distance h1-title white-text text-center"><span class="blue-link"><?php echo _SIDEBAR_CURRENT_TRADE ?></span> | <a href="adminEditedCurrentTrade.php"><?php echo _ADMINCURRENTTRADE_EDITEDCT ?></a> </h1>
    <form action="utilities/adminEditResultFunction.php" method="POST">
    <div id="divFirst" class="width100 overflow blue-opa-bg padding-box radius-box padding-top-bottom0  three-btn-div-padding">
    </div>
		<div id="ss">

		</div>
</div>
</form>
</div>
<?php include 'js.php'; ?>
</body>
</html>
<script type="text/javascript">
$(document).ready(function(){
 $("#divFirst").load("divFirstSec.php");
  // setInterval(function() {
  //      $("#divFirst").load("divFirstSec.php");
  // }, 500);
		// $("#ss").load("testo.php");
		// 	 setInterval(function() {
		// 			 $("#ss").load("testo.php");
		// 	 }, 25000);
    // $("#divSecond").load("divSecondSec.php");
    //    setInterval(function() {
    //        $("#divSecond").load("divSecondSec.php");
    //    }, 1000);
});
</script>
