<div class="width100 same-padding footer-div">
	<p class="footer-p white-text">&copy; <?php echo $time;?> <?php echo _JS_FOOTER ?></p>
</div>

<!-- Login Modal -->
<div id="login-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-login">&times;</span>
    <h1 class="h1-title white-text text-center"><?php echo _JS_LOGIN ?></h1>
         <form action="utilities/loginFunction.php" method="POST">
         	<input class="clean de-input" type="text" placeholder="<?php echo _JS_USERNAME ?>" id="username" name="username" required>
         	<input class="clean de-input" type="password" placeholder="<?php echo _JS_PASSWORD ?>" id="password" name="password" required>
            <!-- <input class="clean" type="checkbox"><label class="white-text"><?php echo _JS_REMEMBER_ME ?></label> -->
            <div class="clear"></div>
            <button class="clean blue-button mid-button-width small-distance small-distance-bottom" name="loginButton"><?php echo _JS_LOGIN ?></button>
            <div class="clear"></div>
            <!-- <a class="open-forgot white-a forgot-a"><?php echo _JS_FORGOT_PASSWORD ?></a> -->
            <div class="width100 text-center"><a class="open-forgot white-a"><?php echo _JS_FORGOT_PASSWORD ?></a></div>
            <div class="clear"></div>
            <div class="distance-20-div"></div>
            <div class="clear"></div>
            <div class="width100 text-center"><a class="open-signup white-a"><?php echo _JS_SIGNUP ?></a></div>
         </form>
  </div>

</div>

<!-- Forgot Password Modal -->
<div id="forgot-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-forgot">&times;</span>
    <h1 class="h1-title white-text text-center"><?php echo _JS_FORGOT_TITLE ?></h1>
    <form>
		<input class="clean  de-input" type="email" placeholder="<?php echo _JS_EMAIL ?>" required name="forgotPassword_email">

        <div class="clear"></div>
        <button class="clean blue-button mid-button-width small-distance small-distance-bottom"><?php echo _JS_SUBMIT ?></button>
        <div class="clear"></div>
        <div class="width100 text-center"><a class="open-login white-a"><?php echo _JS_LOGIN ?></a></div>
    </form>
  </div>

</div>


<!-- Login Modal -->
<div id="signup-modal" class="modal-css">
  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content signup-modal-content">
  <span class="close-css close-signup">&times;</span>
  <h1 class="h1-title white-text text-center"><?php echo _JS_SIGNUP ?></h1>

    <!-- <form> -->
    <form action="utilities/registerFunction.php" method="POST">
      <p class="input-top-text"><?php echo _JS_USERNAME ?></p>
      <input class="clean de-input p6" type="text" placeholder="<?php echo _JS_USERNAME ?>" id="register_username" name="register_username" required>
      <div class="clear"></div>
      <p class="input-top-text"><?php echo _JS_EMAIL ?></p>
      <input class="clean de-input p6" type="email" placeholder="<?php echo _JS_EMAIL ?>" id="register_email_user" name="register_email_user" required>
      <div class="clear"></div>
      <p class="input-top-text"><?php echo _JS_PHONE ?></p>
      <input class="clean de-input p6" type="text" placeholder="<?php echo _JS_PHONE ?>" id="register_phone" name="register_phone" required>
      <div class="clear"></div>
      <div class="dual-input-div">
      <p class="input-top-text"><?php echo _JS_PASSWORD ?></p>
      <input class="clean de-input" type="password" placeholder="<?php echo _JS_PASSWORD ?>" id="register_password" name="register_password" required>
      </div>
      <div class="dual-input-div second-dual-input">
      <p class="input-top-text"><?php echo _JS_RETYPE_PASSWORD ?></p>
      <input class="clean de-input" type="password" placeholder="<?php echo _JS_RETYPE_PASSWORD ?>" id="register_retype_password" name="register_retype_password" required>
      </div>
      <div class="clear"></div>
      <p class="input-top-text"><?php echo _JS_RETYPE_REFERRER_NAME ?></p>
      <input class="clean de-input p6" type="text" placeholder="<?php echo _JS_RETYPE_REFERRER_NAME ?>" id="register_referral_name" name="register_referral_name">
      <div class="clear"></div>
      <button class="clean blue-button width100 small-distance small-distance-bottom" name="loginButton"><?php echo _JS_SIGNUP ?></button>
      <div class="clear"></div>
      <div class="width100 text-center"><a class="open-login white-a"><?php echo _JS_LOGIN ?></a></div>
    </form>

  </div>
</div>


<!-- Buy Modal -->
<div id="buy-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-buy">&times;</span>
    <h1 class="h1-title white-text text-center">AUD/CAD</h1>

        <form>
        	<div class="up-bottom-border">
            	<p class="input-top-text"><?php echo _JS_TYPE ?></p>
            	<select class="clean  de-input">
                	<option selected><?php echo _USERDASHBOARD_BUY ?></option>
                    <option><?php echo _USERDASHBOARD_SELL ?></option>
                </select>
                <p class="input-top-text"><?php echo _USERDASHBOARD_AMOUNT ?></p>
                <input class="clean de-input" type="number" placeholder="<?php echo _USERDASHBOARD_AMOUNT ?>" required>

    		</div>
            <div class="clear"></div>
            <button class="clean blue-button width100 small-distance small-distance-bottom"><?php echo _JS_PLACEORDER ?></button>
            <div class="clear"></div>
        </form>

  </div>

</div>

<!-- Sell Modal -->
<div id="sell-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-sell">&times;</span>
    <h1 class="h1-title white-text text-center">AUD/CAD</h1>

        <form>
        	<div class="up-bottom-border">
            	<p class="input-top-text"><?php echo _JS_TYPE ?></p>
            	<select class="clean  de-input">
                	<option selected><?php echo _USERDASHBOARD_SELL ?></option>
                    <option><?php echo _USERDASHBOARD_BUY ?></option>
                </select>
                <p class="input-top-text"><?php echo _USERDASHBOARD_AMOUNT ?></p>
                <input class="clean de-input" type="number" placeholder="<?php echo _USERDASHBOARD_AMOUNT ?>" required>

    		</div>
            <div class="clear"></div>
            <button class="clean blue-button width100 small-distance small-distance-bottom"><?php echo _JS_PLACEORDER ?></button>
            <div class="clear"></div>
        </form>

  </div>

</div>

<!-- withdraw Modal -->
<!-- <div id="withdraw-modal" class="modal-css"> -->

  <!-- Modal content -->
  <!-- <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-withdraw">&times;</span>
    <h1 class="h1-title white-text text-center"><?php echo _USERDASHBOARD_WITHDRAW ?></h1>

        <form>
        	<div class="up-bottom-border">
            	<p class="input-top-text"><?php echo _USERDASHBOARD_BALANCE ?></p>
            	<p class="clean de-input no-input-style">$1,000,000</p>
                <p class="input-top-text"><?php echo _JS_WITHDRAW_AMOUNT ?></p>
                <input class="clean de-input" type="number" placeholder="<?php echo _USERDASHBOARD_AMOUNT ?>" required>

    		</div>
            <div class="clear"></div>
            <button class="clean blue-button width100 small-distance small-distance-bottom"><?php echo _USERDASHBOARD_WITHDRAW ?></button>
            <div class="clear"></div>
        </form>

  </div>

</div> -->

<!-- Success Modal -->
<div id="success-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-success">&times;</span>
    <div class="clear"></div>
    <h1 class="h1-title white-text text-center mtop-40"><?php echo _JS_SUCCESS ?></h1>
    <div class="width100 text-center">
    	<img src="img/success.png" class="success-icon text-center" alt="<?php echo _JS_SUCCESS ?>" title="<?php echo _JS_SUCCESS ?>">
    </div>
        <div class="clear"></div>
        <div class="clean blue-button mid-button-width small-distance small-distance-bottom close-success text-center width100important"><?php echo _JS_CLOSE ?></div>
        <div class="clear"></div>
  </div>

</div>
<!-- Error Modal -->
<div id="error-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css forgot-modal-content login-modal-content">
    <span class="close-css close-error">&times;</span>
    <div class="clear"></div>
    <h1 class="h1-title white-text text-center mtop-40"><?php echo _JS_ERROR ?></h1>
    <div class="width100 text-center">
    	<img src="img/error.png" class="success-icon text-center" alt="<?php echo _JS_ERROR ?>" title="<?php echo _JS_ERROR ?>">
    </div>
        <div class="clear"></div>
        <div class="clean blue-button mid-button-width small-distance small-distance-bottom close-error text-center width100important"><?php echo _JS_CLOSE ?></div>
        <div class="clear"></div>
  </div>

</div>
<script src="js/jquery-3.2.0.min.js" type="text/javascript"></script>
<script src="js/jquery.fancybox.js" type="text/javascript"></script>  
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/headroom.js"></script>

<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();

    }());
</script>
<script>
$("form").on("change", ".file-upload-field", function(){ 
    $(this).parent(".file-upload-wrapper").attr("data-text",         $(this).val().replace(/.*(\/|\\)/, '') );
});


</script>
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });

    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });

    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;

       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";

    });
    </script>


<!--- Modal Box --->
<script>
var forgotmodal = document.getElementById("forgot-modal");
var loginmodal = document.getElementById("login-modal");
var signupmodal = document.getElementById("signup-modal");
var buymodal = document.getElementById("buy-modal");
var sellmodal = document.getElementById("sell-modal");
var withdrawmodal = document.getElementById("withdraw-modal");
var customerservicemodal = document.getElementById("customerservice-modal");
var successmodal = document.getElementById("success-modal");
var errormodal = document.getElementById("error-modal");

var openforgot = document.getElementsByClassName("open-forgot")[0];
var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var openlogin2 = document.getElementsByClassName("open-login")[2];
var openlogin3 = document.getElementsByClassName("open-login")[3];
var opensignup = document.getElementsByClassName("open-signup")[0];
var opensignup1 = document.getElementsByClassName("open-signup")[1];
var opensignup2 = document.getElementsByClassName("open-signup")[2];
var openbuy = document.getElementsByClassName("open-buy")[0];
var opensell = document.getElementsByClassName("open-sell")[0];
var openwithdraw = document.getElementsByClassName("open-withdraw")[0];
var opencustomerservice = document.getElementsByClassName("open-customerservice")[0];
var opensuccess = document.getElementsByClassName("open-success")[0];
var openerror = document.getElementsByClassName("open-error")[0];

var closeforgot = document.getElementsByClassName("close-forgot")[0];
var closelogin = document.getElementsByClassName("close-login")[0];
var closesignup = document.getElementsByClassName("close-signup")[0];
var closebuy = document.getElementsByClassName("close-buy")[0];
var closesell = document.getElementsByClassName("close-sell")[0];
var closewithdraw = document.getElementsByClassName("close-withdraw")[0];
var closecustomerservice = document.getElementsByClassName("close-customerservice")[0];
var closesuccess = document.getElementsByClassName("close-success")[0];
var closesuccess1 = document.getElementsByClassName("close-success")[1];
var closeerror = document.getElementsByClassName("close-error")[0];
var closeerror1 = document.getElementsByClassName("close-error")[1];
if(openforgot){
openforgot.onclick = function() {
  forgotmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(openlogin){
openlogin.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openlogin1){
openlogin1.onclick = function() {
  loginmodal.style.display = "block";
}
}
if(openlogin2){
openlogin2.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
}
}
if(openlogin3){
openlogin3.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";
}
}
if(opensignup){
opensignup.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
  forgotmodal.style.display = "none";
}
}
if(opensignup1){
opensignup1.onclick = function() {
  signupmodal.style.display = "block";
}
}
if(opensignup2){
opensignup2.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(openbuy){
openbuy.onclick = function() {
  buymodal.style.display = "block";
}
}
if(opensell){
opensell.onclick = function() {
  sellmodal.style.display = "block";
}
}
if(openwithdraw){
openwithdraw.onclick = function() {
  withdrawmodal.style.display = "block";
}
}
if(opencustomerservice){
  opencustomerservice.onclick = function() {
  customerservicemodal.style.display = "block";
}
}
if(opensuccess){
  opensuccess.onclick = function() {
  successmodal.style.display = "block";
  buymodal.style.display = "none";
  sellmodal.style.display = "none";
}
}
if(openerror){
  openerror.onclick = function() {
  errormodal.style.display = "block";
  buymodal.style.display = "none";
  sellmodal.style.display = "none";
}
}
if(closeforgot){
closeforgot.onclick = function() {
  forgotmodal.style.display = "none";
}
}
if(closelogin){
closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}
if(closesignup){
closesignup.onclick = function() {
  signupmodal.style.display = "none";
}
}
if(closebuy){
closebuy.onclick = function() {
  buymodal.style.display = "none";
}
}
if(closesell){
closesell.onclick = function() {
  sellmodal.style.display = "none";
}
}
if(closewithdraw){
closewithdraw.onclick = function() {
  withdrawmodal.style.display = "none";
}
}
if(closecustomerservice){
  closecustomerservice.onclick = function() {
  customerservicemodal.style.display = "none";
}
}
if(closesuccess){
  closesuccess.onclick = function() {
  successmodal.style.display = "none";
}
}
if(closesuccess1){
  closesuccess1.onclick = function() {
  successmodal.style.display = "none";
}
}
if(closeerror){
  closeerror.onclick = function() {
  errormodal.style.display = "none";
}
}
if(closeerror1){
  closesuccess1.onclick = function() {
  errormodal.style.display = "none";
}
}
window.onclick = function(event) {
  if (event.target == forgotmodal) {
    forgotmodal.style.display = "none";
  }
  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }
  if (event.target == signupmodal) {
    signupmodal.style.display = "none";
  }
  if (event.target == buymodal) {
    buymodal.style.display = "none";
  }
  if (event.target == sellmodal) {
    sellmodal.style.display = "none";
  }
  if (event.target == withdrawmodal) {
    withdrawmodal.style.display = "none";
  }
  if (event.target == customerservicemodal) {
    customerservicemodal.style.display = "none";
  }
  if (event.target == successmodal) {
    successmodal.style.display = "none";
  }
  if (event.target == errormodal) {
    errormodal.style.display = "none";
  }
}
</script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>

<script>
function openTab(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" activetab", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " activetab";
}
</script>

<!-- Admin Js -->

<script id="rendered-js">
$("#menu-toggle").click(function (e) {
  e.preventDefault();
  $("#wrapper").toggleClass("toggled");
});
$("#menu-toggle-2").click(function (e) {
  e.preventDefault();
  $("#wrapper").toggleClass("toggled-2");
  $('#menu ul').hide();
});

function initMenu() {
  $('#menu ul').hide();
  $('#menu ul').children('.current').parent().show();
  //$('#menu ul:first').show();
  $('#menu li a').click(
  function () {
    var checkElement = $(this).next();
    if (checkElement.is('ul') && checkElement.is(':visible')) {
      return false;
    }
    if (checkElement.is('ul') && !checkElement.is(':visible')) {
      $('#menu ul:visible').slideUp('normal');
      checkElement.slideDown('normal');
      return false;
    }
  });

}
$(document).ready(function () {
  initMenu();
});
//# sourceURL=pen.js
    </script>
 
<script>
function goBack() {
  window.history.back();
}
</script>
