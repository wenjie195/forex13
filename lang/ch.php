<?php
//for main.js modal files
define("_MAINJS_ATTENTION", "注意");
define("_MAINJS_ENTER_USERNAME", "请输入您的用户名");
define("_MAINJS_ENTER_EMAIL", "请输入您的电子邮件地址");
define("_MAINJS_ENTER_ICNO", "请输入您的身份证号码");
define("_MAINJS_SELECT_COUNTRY", "请选择你的国家");
define("_MAINJS_ENTER_PHONENO", "请输入您的电话号码");
//apply in all
define("_MAINJS_ALL_LOGOUT", "登出");
//index
define("_MAINJS_INDEX_LOGIN", "登录");
define("_MAINJS_INDEX_USERNAME", "用户名");
define("_MAINJS_INDEX_PASSWORD", "密码");
define("_INDEX_BANNER_H11", "便捷交易外汇，");
define("_INDEX_BANNER_H12", "自信地赚钱。");
define("_INDEX_BANNER_P", "与我们一起访问各专业的外汇对，具有优质服务的统一平台。");
define("_INDEX_WHY_TRADE_WITH_US", "为何要与德鑫国际交易外汇？");
define("_INDEX_EXCELLENT_EXE", "优秀的执行力");
define("_INDEX_EXCELLENT_EXE_P", "我们提供更高的填充率、最优惠的价格和减少仓促止损离。");
define("_INDEX_AUTO", "自动化交易");
define("_INDEX_AUTO_P", "没有任何計算程式软件可以操控或改变您的交易。任何滑点都是市场价格自然波动的结果。");
define("_INDEX_UI", "直观的用户界面");
define("_INDEX_UI_P", "我们提供了简易浏览的用户界面方便客户买上和买下外汇。");
define("_INDEX_NO_DEALER", "无需经销商干预");
define("_INDEX_NO_DEALER_P", "我们提供外汇对的实时价格。绝无使用任何计算程序软件更改记录让外汇经纪商获利。");
define("_INDEX_HOW_TRADE", "如何与我们交易？");
define("_INDEX_STEP1", "通过点击注册按钮创建一个交易户口。");
define("_INDEX_STEP2", "填写您的个人资料以完成KYC验证。");
define("_INDEX_STEP3", "向我们存入资金。");
define("_INDEX_STEP4", "一旦资金转入您的账户，您就可以开始交易。");
//adminDashboard
define("_MAINJS_ADMIND_TITLE", "管理员主要版面");
//adminAddReferee.php
define("_MAINJS_ADDREFEREE_TITLE", "添加新用户");
//userDashboard
define("_MAINJS_USERD_TITLE", "用户主要版面");
define("_USERDASHBOARD_USER", "会员");
define("_USERDASHBOARD_PROFILE", "个人头像");
define("_USERDASHBOARD_ACC_NO", "账号号码");
define("_USERDASHBOARD_BALANCE", "余额");
define("_USERDASHBOARD_TRANSACTION", "交易");
define("_USERDASHBOARD_POSITION", "仓位");
define("_USERDASHBOARD_WIN", "赢盘");
define("_USERDASHBOARD_LOSS", "输盘");
define("_USERDASHBOARD_SEARCH", "搜索");
define("_USERDASHBOARD_SELL", "买跌");
define("_USERDASHBOARD_BUY", "买涨");
define("_USERDASHBOARD_CHANGE", "变化");
define("_USERDASHBOARD_PERCENTAGE_CHANGE", "%变化");
define("_USERDASHBOARD_INSTRUMENT", "产品");
define("_USERDASHBOARD_AMOUNT", "金额");
define("_USERDASHBOARD_TIMELEFT", "剩余时间");
define("_USERDASHBOARD_VI_FX_TRADE", "外汇交易（3）");
define("_USERDASHBOARD_WITHDRAW", "提现");
define("_USERDASHBOARD_VIEW_MESSAGE", "查看信息");
define("_USERDASHBOARD_CUSTOMER_SERVICE", "客服");
define("_USERDASHBOARD_TIMELINE", "时间线");
define("_USERDASHBOARD_SEC", "秒");
define("_USERDASHBOARD_OR_AMOUNT", "或者在这里输入金额");
define("_USERDASHBOARD_BUY_DOWN", "买跌");
define("_USERDASHBOARD_BUY_UP", "买涨");
define("_USERDASHBOARD_TIME_AMOUNT", "请选择时间线和金额");
define("_USERDASHBOARD_TIME", "时间线");
define("_USERDASHBOARD_OTHERAMOUNT", "其它金额");
define("_USERDASHBOARD_SUBMIT", "提交");
define("_USERDASHBOARD_BN", "银行名称");
define("_USERDASHBOARD_BENEFICIALNAME", "受益人名字");
define("_USERDASHBOARD_BAN", "银行账号");
define("_USERDASHBOARD_WITHDRAWAL", "提交");
define("_USERDASHBOARD_TRADE_RECORD", "交易记录");
define("_USERDASHBOARD_DASHBOARD_PAGE", "首页");
define("_USERDASHBOARD_LANGUAGE", "语言");
define("_USERDASHBOARD_PRODUCT", "商品");
define("_USERDASHBOARD_CURRENT_PRICE", "现价");
define("_USERDASHBOARD_AMOUNT2", "选择金额");
define("_USERDASHBOARD_OTHERAMOUNT2", "或输入其他金额在这里");
define("_USERDASHBOARD_CLICK_TO_SENT", "点击发出");
//header
define("_HEADER_DEXINGUOJI", "德鑫国际");
define("_HEADER_LANGUAGE", "Language/语言");
define("_HEADER_LOGOUT", "登出");
define("_HEADER_PROFILE", "个人中心");
define("_HEADER_EDIT_PROFILE", "修改个人资料");
define("_HEADER_CHANGE_EMAIL", "更改电邮地址");
define("_HEADER_CHANGE_PHONE_NO", "更改手机号码");
define("_HEADER_CHANGE_PASSWORD", "更改密码");
define("_HEADER_SIGN_UP", "注册");
define("_HEADER_LOGIN", "登录");
define("_HEADER_MESSAGE", "信息");
//JS
define("_JS_FOOTER", "德鑫国际版权所有");
define("_JS_LOGIN", "登入");
define("_JS_USERNAME", "用户名");
define("_JS_PASSWORD", "密码");
define("_JS_FULLNAME", "全名");
define("_JS_NEW_PASSWORD", "新密码");
define("_JS_CURRENT_PASSWORD", "现在的密码");
define("_JS_RETYPE_PASSWORD", "再次输入密码");
define("_JS_RETYPE_REFERRER_NAME", "推荐人名字");
define("_JS_REMEMBER_ME", "记住我");
define("_JS_FORGOT_PASSWORD", "忘记密码");
define("_JS_FORGOT_TITLE", "忘记密码");
define("_JS_EMAIL", "邮箱地址");
define("_JS_SIGNUP", "注册");
define("_JS_FIRSTNAME", "名字");
define("_JS_LASTNAME", "姓氏");
define("_JS_GENDER", "性别");
define("_JS_MALE", "男");
define("_JS_FEMALE", "女");
define("_JS_BIRTHDAY", "出生日期");
define("_JS_COUNTRY", "国家");
define("_JS_MALAYSIA", "马来西亚");
define("_JS_SINGAPORE", "新加坡");
define("_JS_PHONE", "电话号码");
define("_JS_REQUEST_TAC", "申请TAC");
define("_JS_TYPE", "类型");
define("_JS_SUBMIT", "提交");
define("_JS_PLACEORDER", "下单");
define("_JS_WITHDRAW_AMOUNT", "提现金额");
define("_JS_SUCCESS", "交易成功！");
define("_JS_CLOSE", "关闭");
define("_JS_ERROR", "错误");
//SIDEBAR
define("_SIDEBAR_DASHBOARD", "概览");
define("_SIDEBAR_ACC_CREATION", "创建账户");
define("_SIDEBAR_CUSTOMER_LIST", "客户名单");
define("_SIDEBAR_WITHDRAW_REQUEST", "提款请求");
define("_SIDEBAR_CURRENT_TRADE", "当前交易");
define("_SIDEBAR_TOTAL_PROFIT", "利润/亏损");
//ADMIN DASHBOARD
define("_ADMINDASHBOARD_NO_OF_WIN", "赢盘");
define("_ADMINDASHBOARD_TOTAL_WIN", "总利润");
define("_ADMINDASHBOARD_NO_OF_LOSS", "输盘");
define("_ADMINDASHBOARD_TOTAL_LOSS", "总亏损");
define("_ADMINDASHBOARD_WITHDRAW_REQUEST", "提款请求");
//ACCOUNT CREATION
define("_ACC_CREATION_ACC_TYPE", "户口类型");
define("_ACC_CREATION_CUSTOMER", "客户");
define("_ACC_CREATION_ADMIN", "管理员");
define("_ACC_CREATION_TOPUP", "充值");
//ACCOUNT CREATION
define("_VIEWMESSAGE_VIEW_ALL_MESSAGE", "浏览所有信息");
define("_VIEWMESSAGE_NO", "序");
define("_VIEWMESSAGE_SENT", "已发出");
define("_VIEWMESSAGE_REPLY", "回复");
define("_VIEWMESSAGE_DATE", "日期");
define("_VIEWMESSAGE_MESSAGE_STATUS", "状态");
define("_VIEWMESSAGE_READ", "已读");
define("_VIEWMESSAGE_NEW_MESSAGE", "新信息");
define("_VIEWMESSAGE_CHOOSE_YOUR_FILE", "选文件上传");
define("_VIEWMESSAGE_MUTE", "关闭信息提示声");
//WITHDRAWAL
define("_WITHDRAWAL_TIME", "申请时间");
define("_WITHDRAWAL_VERIFY", "提款状态");
//ADMIN VERIFY WITHDRAWAL
define("_ADMINVERIFYWITHDRAWAL_WITHDRAWAL_NO", "提款序号 : #");
define("_ADMINVERIFYWITHDRAWAL_BANK_NAME", "银行");
define("_ADMINVERIFYWITHDRAWAL_WITHDRAWAL_AMOUNT", "提款数额");
define("_ADMINVERIFYWITHDRAWAL_DATE_AND_TIME", "日期和时间");
define("_ADMINVERIFYWITHDRAWAL_REFERENCE", "备注");
define("_ADMINVERIFYWITHDRAWAL_REJECT", "拒绝");
define("_ADMINVERIFYWITHDRAWAL_ACCEPTED", "同意");
define("_ADMINVERIFYWITHDRAWAL_TITLE", "提款验证");
//ADMIN WITHDRAWAL
define("_ADMINWITHDRAWAL_USERNAME", "用户名");
define("_ADMINWITHDRAWAL_AMOUNT", "数额");
define("_ADMINWITHDRAWAL_REQUEST_TIME", "申请时间");
define("_ADMINWITHDRAWAL_VERIFY", "审核");
//ADMIN CURRENT TRADE
define("_ADMINCURRENTTRADE_UNREALIZED_GAIN", "顾客输");
define("_ADMINCURRENTTRADE_UNREALIZED_LOSS", "顾客赢");
define("_ADMINCURRENTTRADE_EDITEDCT", "已更改当前交易");
//ADMIN WITHDRAWAL
define("_ADMINWITHDRAWAL_COMPLETE", "完成");
define("_ADMINWITHDRAWAL_REJECT", "拒绝");
//ADMIN TOTAL PROFIT/LOSS
define("_ADMINTOTALPROFITLOSS_TOTAL_PROFIT_USD", "总利润 （美金）");
define("_ADMINTOTALPROFITLOSS_TOTAL_LOSS_USD", "总亏损 （美金）");
define("_ADMINTOTALPROFITLOSS_DATE", "日期");
define("_ADMINTOTALPROFITLOSS_TIMEFRAME", "时间线");
define("_ADMINTOTALPROFITLOSS_CUSTOMER_NAME", "客户名字");
define("_ADMINTOTALPROFITLOSS_CURRENCY_PAIR", "汇率兑换");
//ADMIN MEMBER
define("_ADMINMEMBER_CREDIT", "余额");
define("_ADMINMEMBER_ADDCREDIT", "充值");
define("_ADMINMEMBER_TOPUPCREDIT", "点击充值");
define("_ADMINMEMBER_LASTUP", "最近更新时间");
define("_ADMINMEMBER_DETAILS", "详情");
define("_ADMINMEMBER_CLICKDETAILS", "查看详情");
define("_ADMINMEMBER_OPERATION", "操作");
//adminUserAddCredit
define("_AUACCURRENTCREDIT_CCREDIT", "目前余额");
define("_AUACCURRENTCREDIT_TUAMOUNT", "充值数额");
define("_AUACCURRENTCREDIT_SUBMIT", "提交");
//adminUserAddCredit
define("_AUD_CUSDETAILA", "客户资料");
define("_AUD_DEPOSITHIS", "充值历史");
define("_AUD_ACCOUNTNO", "银行户口号码");
define("_AUD_AMOUNT", "数额");
define("_AUD_TOPUP", "充值");
define("_AUD_CLICKTOPUP", "点击充值");
define("_AUD_ACCSTATUS", "户口状态");
define("_AUD_ACTIVE", "激活");
define("_AUD_INACTIVE", "停用");
define("_AUD_TOPUPBY", "充值负责人");
define("_AUD_DATE", "日期");
//adminUserTradeDetails
define("_AUTD_NOOFWIN", "赢盘");
define("_AUTD_NOOFLOSE", "输盘");
define("_AUTD_NEETGL", "净收益/损失");
define("_AUTD_WIN", "赢盘");
define("_AUTD_LOSE", "输盘");
define("_AUTD_TOPUP", "充值");
define("_AUTD_WITHDRAWAL", "提现");
//Profile
define("_PROFILE_PERSONAL_DETAILS", "个人资料");
define("_PROFILE_TOPUP_HISTORY", "充值记录");
define("_PROFILE_WITHDRAW_HISTORY", "提现记录");
//Top Up History
define("_TOPUP_HISTORY_DATE", "日期");
//ViewMessage
define("_VIEWMESSAGE_SENT2", "发出");
define("_VIEWMESSAGE_UR_MESSAGE", "输入你的信息");
define("_VIEWMESSAGE_UPLOAD", "上载照片");
define("_VIEWMESSAGE_JUST_UPLOAD", "上载");
define("_VIEWMESSAGE_SENT3", "上载");
//Profile
define("_PROFILE_CHOOSE_COUNTRY", "选择国家");