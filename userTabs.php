<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/timezone.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$date = date('YmdHi');
$datetime = DateTime::createFromFormat('YmdHi', $date);
$dayName = $datetime->format('D');
$newCurrentTime = date('Hi');
if ($dayName == 'Sat' && $newCurrentTime >= '0500'  || $dayName == 'Sun' || $dayName == 'Mon' && $newCurrentTime < '0600') {
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://financialmodelingprep.com/api/v3/forex",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"x-rapidapi-host: currency-exchange.p.rapidapi.com",
			"x-rapidapi-key: c5875e8297msh333e08626d58b3fp1015afjsn73bce78f515f"
		),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
		echo "cURL Error #:" . $err;
	} else {
		$exchangeRates = json_decode($response, true);
	}

	?>
			<table class="table-width data-table">
	        	<thead>
	            	<tr>
						<th><?php echo _USERDASHBOARD_PRODUCT ?></th>
						<th></th>
	                    <th><?php echo _USERDASHBOARD_BUY ?></th>
	                    <th><?php echo _USERDASHBOARD_SELL ?></th>
	                    <th><?php echo _USERDASHBOARD_CHANGE ?></th>

	                </tr>
	            </thead>
	            <tbody>
					<?php
					if ($exchangeRates)
					{

								if($_SESSION['lang'] == 'en' || isset($_GET['lang']) == 'en' )
								{
									$currencyArray = ("EUR/USD,USD/CAD,GBP/JPY,AUD/USD,USD/JPY,GBP/USD,USD/CHF,EUR/AUD,EUR/JPY,EUR/GBP,AUD/JPY,NZD/USD,EUR/CHF,GBP/CAD,CAD/JPY");
								}
								else
								{
									$currencyArray = ("欧元/美元,美元/加拿大元,英镑/日元,澳大利亚元/美元,美元/日元,英镑/美元,美元/瑞士法郎,欧元/澳大利亚元,欧元/日元,欧元/英镑,澳大利亚元/日元,新西兰元/美元,欧元/瑞士法郎,英镑/加拿大元,加拿大元/日元");
								}

								// $currencyArray = ("欧元/美元,美元/加拿大元,英镑/日元,澳大利亚元/美元,美元/日元,英镑/美元,美元/瑞士法郎,欧元/澳大利亚元,欧元/日元,欧元/英镑,澳大利亚元/日元,新西兰元/美元,欧元/瑞士法郎,英镑/加拿大元,加拿大元/日元");

								$imageArray = ('<img src="img/europe.png" alt="Europe" title="Europe" class="country-flag">,
												<img src="img/us.png" alt="US" title="US" class="country-flag">,
												<img src="img/british.png" alt="British" title="British" class="country-flag">,
												<img src="img/australia.png" alt="Australia" title="Australia" class="country-flag">,
												<img src="img/us.png" alt="US" title="US" class="country-flag">,
												<img src="img/british.png" alt="British" title="British" class="country-flag">,
												<img src="img/us.png" alt="US" title="US" class="country-flag">,
												<img src="img/europe.png" alt="Europe" title="Europe" class="country-flag">,
												<img src="img/europe.png" alt="Europe" title="Europe" class="country-flag">,
												<img src="img/europe.png" alt="Europe" title="Europe" class="country-flag">,
												<img src="img/australia.png" alt="Australia" title="Australia" class="country-flag">,
												<img src="img/new-zealand.png" alt="Australia" title="Australia" class="country-flag">,
												<img src="img/europe.png" alt="Australia" title="Australia" class="country-flag">,
												<img src="img/british.png" alt="British" title="British" class="country-flag">,
												<img src="img/canada.png" alt="Canada" title="Canada" class="country-flag">,');

								$imageArrayExplode = explode(",",$imageArray);

								$image2Array = ('<img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
												<img src="img/canada.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
												<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
												<img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
												<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
												<img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
												<img src="img/swiss-franc.png" alt="Switzerland" title="Switzerland" class="country-flag country-flag2">,
												<img src="img/australia.png" alt="Australia" title="Australia" class="country-flag country-flag2">,
												<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
												<img src="img/british.png" alt="British" title="British" class="country-flag country-flag2">,
												<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
												<img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
												<img src="img/swiss-franc.png" alt="Switzerland" title="Switzerland" class="country-flag country-flag2">,
												<img src="img/canada.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
												<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,');

								$image2ArrayExplode = explode(",",$image2Array);
							$currencyArrayWithSlash = ("EUR/USD,USD/CAD,GBP/JPY,AUD/USD,USD/JPY,GBP/USD,USD/CHF,EUR/AUD,EUR/JPY,EUR/GBP,AUD/JPY,NZD/USD,EUR/CHF,GBP/CAD,CAD/JPY");

						$currencyArrayExplode = explode(",",$currencyArray);
						$currencyArrayWithSlashExplode = explode(",",$currencyArrayWithSlash);
						?><input type="hidden" id="total" value="<?php echo count($currencyArrayExplode); ?>"> <?php
						for ($cntAA=0; $cntAA <count($currencyArrayExplode) ; $cntAA++)
						{
							for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
							{
								$selectCountry = str_replace("C:","",$exchangeRates['forexList'][$cnt]['ticker']);
								if ($selectCountry == $currencyArrayWithSlashExplode[$cntAA])
								{
									?>
									<tr class="open-buy">
										<td ><?php echo $imageArrayExplode[$cntAA]; ?> <?php  echo $currencyArrayExplode[$cntAA] ; ?><?php echo $image2ArrayExplode[$cntAA]; ?></td>

										<td ></td>
	<td>
		<button  id="<?php echo "currency_nameDisable".$cntAA ?>" value="<?php echo  $currencyArrayWithSlashExplode[$cntAA]?>" class="clean blue-button open-buy green-btn table-btn-font fix-100">
			<?php echo number_format($exchangeRates['forexList'][$cnt]['ask'], 4); ?>
		</button>
	</td>
	<td>
		<button  id="<?php echo "currency_nameSellDisable".$cntAA ?>" value="<?php echo  $currencyArrayWithSlashExplode[$cntAA]?>"  class="clean blue-button open-sell red-btn table-btn-font fix-100" >
			<?php   echo number_format($exchangeRates['forexList'][$cnt]['bid'], 4); ?>
		</button>
	</td>

										<?php if ($exchangeRates['forexList'][$cnt]['changes'] < 0)
										{
										?>
											<td class="red-text"><?php   echo number_format($exchangeRates['forexList'][$cnt]['changes'], 4)."<br>"; ?>
										<?php
										}
										elseif ($exchangeRates['forexList'][$cnt]['changes'] >= 0)
										{
										?>
											<td class="green-text"><?php   echo number_format($exchangeRates['forexList'][$cnt]['changes'], 4)."<br>"; ?>
										<?php
										}
										?>

									</tr>
									<?php
								}
							}
						}
					}
					?>
	            </tbody>
			</table>
<?php
}else {
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => "https://financialmodelingprep.com/api/v3/forex",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"x-rapidapi-host: currency-exchange.p.rapidapi.com",
			"x-rapidapi-key: c5875e8297msh333e08626d58b3fp1015afjsn73bce78f515f"
		),
	));

	$response = curl_exec($curl);
	$err = curl_error($curl);

	curl_close($curl);

	if ($err) {
		echo "cURL Error #:" . $err;
	} else {
		$exchangeRates = json_decode($response, true);
	}

	?>
			<table class="table-width data-table">
	        	<thead>
	            	<tr>
						<th><?php echo _USERDASHBOARD_PRODUCT ?></th>
						<th></th>
	                    <th><?php echo _USERDASHBOARD_BUY ?></th>
	                    <th><?php echo _USERDASHBOARD_SELL ?></th>
	                    <th><?php echo _USERDASHBOARD_CHANGE ?></th>

	                </tr>
	            </thead>
	            <tbody>
					<?php
					if ($exchangeRates)
					{

								if($_SESSION['lang'] == 'en' || isset($_GET['lang']) == 'en' )
								{
									$currencyArray = ("EUR/USD,USD/CAD,GBP/JPY,AUD/USD,USD/JPY,GBP/USD,USD/CHF,EUR/AUD,EUR/JPY,EUR/GBP,AUD/JPY,NZD/USD,EUR/CHF,GBP/CAD,CAD/JPY");
								}
								else
								{
									$currencyArray = ("欧元/美元,美元/加拿大元,英镑/日元,澳大利亚元/美元,美元/日元,英镑/美元,美元/瑞士法郎,欧元/澳大利亚元,欧元/日元,欧元/英镑,澳大利亚元/日元,新西兰元/美元,欧元/瑞士法郎,英镑/加拿大元,加拿大元/日元");
								}

								// $currencyArray = ("欧元/美元,美元/加拿大元,英镑/日元,澳大利亚元/美元,美元/日元,英镑/美元,美元/瑞士法郎,欧元/澳大利亚元,欧元/日元,欧元/英镑,澳大利亚元/日元,新西兰元/美元,欧元/瑞士法郎,英镑/加拿大元,加拿大元/日元");

								$imageArray = ('<img src="img/europe.png" alt="Europe" title="Europe" class="country-flag">,
												<img src="img/us.png" alt="US" title="US" class="country-flag">,
												<img src="img/british.png" alt="British" title="British" class="country-flag">,
												<img src="img/australia.png" alt="Australia" title="Australia" class="country-flag">,
												<img src="img/us.png" alt="US" title="US" class="country-flag">,
												<img src="img/british.png" alt="British" title="British" class="country-flag">,
												<img src="img/us.png" alt="US" title="US" class="country-flag">,
												<img src="img/europe.png" alt="Europe" title="Europe" class="country-flag">,
												<img src="img/europe.png" alt="Europe" title="Europe" class="country-flag">,
												<img src="img/europe.png" alt="Europe" title="Europe" class="country-flag">,
												<img src="img/australia.png" alt="Australia" title="Australia" class="country-flag">,
												<img src="img/new-zealand.png" alt="Australia" title="Australia" class="country-flag">,
												<img src="img/europe.png" alt="Australia" title="Australia" class="country-flag">,
												<img src="img/british.png" alt="British" title="British" class="country-flag">,
												<img src="img/canada.png" alt="Canada" title="Canada" class="country-flag">,');

								$imageArrayExplode = explode(",",$imageArray);

								$image2Array = ('<img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
												<img src="img/canada.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
												<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
												<img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
												<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
												<img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
												<img src="img/swiss-franc.png" alt="Switzerland" title="Switzerland" class="country-flag country-flag2">,
												<img src="img/australia.png" alt="Australia" title="Australia" class="country-flag country-flag2">,
												<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
												<img src="img/british.png" alt="British" title="British" class="country-flag country-flag2">,
												<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
												<img src="img/us.png" alt="US" title="US" class="country-flag country-flag2">,
												<img src="img/swiss-franc.png" alt="Switzerland" title="Switzerland" class="country-flag country-flag2">,
												<img src="img/canada.png" alt="Canada" title="Canada" class="country-flag country-flag2">,
												<img src="img/japan.png" alt="Canada" title="Canada" class="country-flag country-flag2">,');

								$image2ArrayExplode = explode(",",$image2Array);
							$currencyArrayWithSlash = ("EUR/USD,USD/CAD,GBP/JPY,AUD/USD,USD/JPY,GBP/USD,USD/CHF,EUR/AUD,EUR/JPY,EUR/GBP,AUD/JPY,NZD/USD,EUR/CHF,GBP/CAD,CAD/JPY");

						$currencyArrayExplode = explode(",",$currencyArray);
						$currencyArrayWithSlashExplode = explode(",",$currencyArrayWithSlash);
						?><input type="hidden" id="total" value="<?php echo count($currencyArrayExplode); ?>"> <?php
						for ($cntAA=0; $cntAA <count($currencyArrayExplode) ; $cntAA++)
						{
							for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
							{
								$selectCountry = str_replace("C:","",$exchangeRates['forexList'][$cnt]['ticker']);
								if ($selectCountry == $currencyArrayWithSlashExplode[$cntAA])
								{
									?>
									<tr class="open-buy">
										<td ><?php echo $imageArrayExplode[$cntAA]; ?> <?php  echo $currencyArrayExplode[$cntAA] ; ?><?php echo $image2ArrayExplode[$cntAA]; ?></td>

										<td ></td>
	<td>
		<button id="<?php echo "currency_name".$cntAA ?>" value="<?php echo  $currencyArrayWithSlashExplode[$cntAA]?>" class="clean blue-button open-buy green-btn table-btn-font fix-100">
			<?php echo number_format($exchangeRates['forexList'][$cnt]['ask'], 4); ?>
		</button>
	</td>
	<td>
		<button id="<?php echo "currency_nameSell".$cntAA ?>" value="<?php echo  $currencyArrayWithSlashExplode[$cntAA]?>"  class="clean blue-button open-sell red-btn table-btn-font fix-100" >
			<?php   echo number_format($exchangeRates['forexList'][$cnt]['bid'], 4); ?>
		</button>
	</td>

										<?php if ($exchangeRates['forexList'][$cnt]['changes'] < 0)
										{
										?>
											<td class="red-text"><?php   echo number_format($exchangeRates['forexList'][$cnt]['changes'], 4)."<br>"; ?>
										<?php
										}
										elseif ($exchangeRates['forexList'][$cnt]['changes'] >= 0)
										{
										?>
											<td class="green-text"><?php   echo number_format($exchangeRates['forexList'][$cnt]['changes'], 4)."<br>"; ?>
										<?php
										}
										?>

									</tr>
									<?php
								}
							}
						}
					}
					?>
	            </tbody>
			</table>
<?php
}
?>
				<script type="text/javascript">
				var total = $("#total").val();
				for (var i = 0; i < total; i++)
				{
					$('#currency_name'+i+'').click( function()
					{
						var currenyName = $(this).val();
						$("#buy-modal").fadeIn(function(){
							$("#buy-modal").show();
							$("#trade_type").empty();
							$("#trade_type").append("<option>BUY</option>")
						} );
						$("#getCurrenyName").html("<a>"+currenyName+"</a>");
						$("#getCurrency").append("<input type='hidden' name='trade_currency' value="+currenyName+">");
					});

					$('#currency_nameSell'+i+'').click( function()
					{
						var currenyName = $(this).val();
						$("#buy-modal").fadeIn(function(){
							$("#buy-modal").show();
							$("#trade_type").empty();
							$("#trade_type").append("<option>SELL</option>")
						} );
						$("#getCurrenyName").html("<a>"+currenyName+"</a>");
						$("#getCurrency").append("<input type='hidden' name='trade_currency' value="+currenyName+">");
					});
				}
				</script>
				<script type="text/javascript">
				var total = $("#total").val();
				for (var i = 0; i < total; i++)
				{
					$('#currency_nameDisable'+i+'').click( function()
					{
						var currenyName = $(this).val();
						$("#disable-modal").fadeIn(function(){
							$("#disable-modal").show();
						} );
						$("#getCurrenyName").html("<a>"+currenyName+"</a>");
						$("#getCurrency").append("<input type='hidden' name='trade_currency' value="+currenyName+">");
					});

					$('#currency_nameSellDisable'+i+'').click( function()
					{
						var currenyName = $(this).val();
						$("#disable-modal").fadeIn(function(){
							$("#disable-modal").show();
						} );
						$("#getCurrenyName").html("<a>"+currenyName+"</a>");
						$("#getCurrency").append("<input type='hidden' name='trade_currency' value="+currenyName+">");
					});
				}
				</script>
