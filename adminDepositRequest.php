<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Deposit.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $orderDetailsAAA = getDeposit($conn," WHERE status = 'PENDING' ");
$depositDetails = getDeposit($conn," WHERE status = 'PENDING' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<!-- <?php //include 'meta.php'; ?> -->
    <meta property="og:url" content="https://dxforextrade88.com/adminDepositRequest.php" />
    <meta property="og:title" content="Deposit Request| De Xin Guo Ji 德鑫国际" />
    <title>Deposit Request| De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminDepositRequest.php" />
	<!-- <?php //include 'css.php'; ?> -->
</head>
<body class="body">

<?php //echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<!-- <?php //include 'adminSidebar.php'; ?> -->

<div class="next-to-sidebar">
    <h1 class="h1-title">Deposit Request</h1>

    <div class="clear"></div>

<div class="width100 overflow">
    <?php $conn = connDB();?>
    <table class="shipping-table">    
        <thead>
            <tr>
                <th>NO.</th>
                <!-- <th>ORDER NUMBER</th> -->
                <th>USERNAME</th>
                <th>AMOUNT</th>
                <th>DEPOSIT DATE TIME</th>
                <th>VERIFY</th>
            </tr>
        </thead>
        <tbody>
            <?php
            if($depositDetails != null)
            {
            for($cnt = 0;$cnt < count($depositDetails) ;$cnt++)
            {?>
            <tr>
                <td><?php echo ($cnt+1)?></td>
                <!-- <td>#<?php //echo $depositDetails[$cnt]->getId();?></td> -->
                <td><?php echo $depositDetails[$cnt]->getUsername();?></td>
                <td><?php echo $depositDetails[$cnt]->getAmount();?></td>
                <td><?php $dateCreated = date("Y-m-d",strtotime($depositDetails[$cnt]->getDateCreated()));
                    echo $dateCreated;?>
                </td>
                <td>
                <!-- <form action="depositVerification.php" method="POST"> -->
                <form action="adminDepositVerification.php" method="POST">
                    <button class="clean edit-anc-btn hover1" type="submit" name="deposit_id" value="<?php echo $depositDetails[$cnt]->getId();?>">
                        <img src="img/verify-payment.png" class="edit-announcement-img hover1a" alt="Verify Deposit" title="Verify Deposit">
                        <img src="img/verify-payment2.png" class="edit-announcement-img hover1b" alt="Verify Deposit" title="Verify Deposit">
                    </button>
                </form>
                </td>
            </tr>
            <?php
            }
            }
            ?>
        </tbody>
    </table>
    <?php $conn->close();?>
</div>

    <div class="clear"></div>
</div>

<style>
.account-li{
	color:#bf1b37;
	background-color:white;}
.account-li .hover1a{
	display:none;}
.account-li .hover1b{
	display:block;}
</style>

<!-- <?php //include 'js.php'; ?> -->

</body>
</html>