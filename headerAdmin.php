<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <img src="img/logo3.png" class="logo-img" alt="<?php echo _HEADER_DEXINGUOJI ?>" title="<?php echo _HEADER_DEXINGUOJI ?>">
            </div>
            <div class="right-menu-div float-right">
                <div class="dropdown hover1 menu-item menu-a">
                	<?php echo _HEADER_LANGUAGE ?>	
                            	<img src="img/dropdown.png" class="dropdown-img hover1a" alt="<?php echo _HEADER_LANGUAGE ?>" title="<?php echo _HEADER_LANGUAGE ?>">
                                <img src="img/dropdown2.png" class="dropdown-img hover1b" alt="<?php echo _HEADER_LANGUAGE ?>" title="<?php echo _HEADER_LANGUAGE ?>">
                	<div class="dropdown-content yellow-dropdown-content">
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en" class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch" class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p>
                	</div>
                </div>
                
                <a href="adminViewMessage.php" class="menu-item menu-a menu-padding"><div><?php echo _HEADER_MESSAGE ?></div></a>

                <!-- <a href="adminViewMessage.php" class="menu-item menu-a menu-padding"><div><?php //echo _HEADER_MESSAGE ?><div class="menu-red-dot"></div></div></a> -->
                <a href="logout.php" class="menu-padding menu-item menu-a"><?php echo _MAINJS_ALL_LOGOUT ?></a>
					
                    <div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
                            <li><a href="<?php $link ?>?lang=en">English</a></li>
                            <li><a href="<?php $link ?>?lang=ch">中文</a></li>
                            <li><a  href="adminViewMessage.php"><?php echo _HEADER_MESSAGE ?><div class="menu-red-dot"></div></a></li>
                            <li><a href="logout.php"><?php echo _HEADER_LOGOUT ?></a></li>                                                                                                    
						</ul>
					</div>                 
            </div>
        </div>
</header>