<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$withdrawalDetails = getWithdrawal($conn," WHERE status = 'PENDING' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminVerifyWithdrawal.php" />
    <meta property="og:title" content="Verify Withdrawal | De Xin Guo Ji 德鑫国际" />
    <title>Verify Withdrawal | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminVerifyWithdrawal.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
	<?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2">
        <!-- <h1 class="menu-distance h1-title white-text text-center">Verify Withdrawal</h1> -->
        <h1 class="menu-distance h1-title white-text text-center"><?php echo _ADMINVERIFYWITHDRAWAL_TITLE ?></h1>
        <div class="width100 overflow blue-opa-bg padding-box radius-box">
    
            <h1 class="h1-title" onclick="goBack()">
                <a class="black-white-link2 hover1">
                    <?php echo _ADMINVERIFYWITHDRAWAL_WITHDRAWAL_NO ?><?php echo $_POST['withdrawal_id'];?>
                </a>
            </h1>
    
            <form method="POST" id="WithdrawalVerifiedForm" onsubmit="doPreview(this.submited); return false;">
                <div class="width100 shipping-div2">
                    <table class="withdraw-table">
                        <tbody>
                        <?php
                        if(isset($_POST['withdrawal_id']))
                        {
                            $conn = connDB();
                            //Order
                            $depositArray = getWithdrawal($conn,"WHERE id = ? ", array("id") ,array($_POST['withdrawal_id']),"i");
    
                            if($depositArray != null)
                            {
                                ?>
                                <tr>
                                    <td><?php echo _MAINJS_INDEX_USERNAME ?></td>
                                    <td>:</td>
                                    <td><?php echo $depositArray[0]->getUsername()?></td>
                                </tr>
                                <tr>
                                    <td><?php echo _ADMINVERIFYWITHDRAWAL_BANK_NAME ?></td>
                                    <td>:</td>
                                    <td><?php echo $depositArray[0]->getBankName()?></td>
                                </tr>
                                <tr>
                                    <td><?php echo _ADMINVERIFYWITHDRAWAL_WITHDRAWAL_AMOUNT ?></td>
                                    <td>:</td>
                                    <td><?php echo $depositArray[0]->getAmount()?></td>
                                </tr>
                                <tr>
                                    <td><?php echo _ADMINVERIFYWITHDRAWAL_DATE_AND_TIME ?></td>
                                    <td>:</td>
                                    <td><?php echo $depositArray[0]->getDateCreated();?></td>
                                </tr>
                                <tr>
                                    <td><?php echo _ADMINVERIFYWITHDRAWAL_REFERENCE ?></td>
                                    <td>:</td>
                                    <td>
                                        <input type="text" id="withdrawal_reference" name="withdrawal_reference" placeholder="<?php echo _ADMINVERIFYWITHDRAWAL_REFERENCE ?>" class="de-input min-width-input">
                                    </td>
                                </tr>
                            <?php
                            }
                        }?>
                        </tbody>
                    </table>
                </div>
    
                <input type="hidden" id="withdrawal_id" name="withdrawal_id" value="<?php echo $depositArray[0]->getId()?>">
                <input type="hidden" id="withdrawal_uid" name="withdrawal_uid" value="<?php echo $depositArray[0]->getUid()?>">
                <input type="hidden" id="withdrawal_username" name="withdrawal_username" value="<?php echo $depositArray[0]->getUsername()?>">
                <input type="hidden" id="withdrawal_amount" name="withdrawal_amount" value="<?php echo $depositArray[0]->getAmount()?>">
    
                <div class="clear"></div>
    
                <!-- <div class="three-btn-container">
                    <input onclick="this.form.submited=this.value;"  type="submit" name="REJECT" value="<?php //echo _ADMINVERIFYWITHDRAWAL_REJECT ?>" class="clean blue-button left-btn-width1 red-btn">
                    <input onclick="this.form.submited=this.value;"  type="submit" name="ACCEPTED" value="<?php //echo _ADMINVERIFYWITHDRAWAL_ACCEPTED ?>" class="clean blue-button right-btn-width1">
                </div> -->

                <div class="three-btn-container">
                    <input onclick="this.form.submited=this.value;"  type="submit" name="REJECT" value="REJECT" class="clean blue-button left-btn-width1 red-btn">
                    <input onclick="this.form.submited=this.value;"  type="submit" name="ACCEPTED" value="ACCEPTED" class="clean blue-button right-btn-width1">
                </div>
    
                </form>
            </div>
	</div>
</div>
<?php include 'js.php'; ?>

<script type="text/javascript">
    function doPreview(buttonType)
    {
        switch(buttonType)
        {
            case 'ACCEPTED':
                form=document.getElementById('WithdrawalVerifiedForm');
                form.action='utilities/withdrawalAcceptedFunction.php';
                form.submit();
            break;
            case 'REJECT':
                form=document.getElementById('WithdrawalVerifiedForm');
                form.action='utilities/withdrawalRejectFunction.php';
                form.submit();
            break;
        }
    }
</script>

</body>
</html>