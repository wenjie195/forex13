<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <img src="img/logo3.png" class="logo-img" alt="<?php echo _HEADER_DEXINGUOJI ?>" title="<?php echo _HEADER_DEXINGUOJI ?>">
            </div>

            <?php
                $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
                $_SERVER['REQUEST_URI'];
            ?>

            <div class="right-menu-div float-right">
                <div class="dropdown hover1 menu-item menu-a">
                    <!-- <?php echo _HEADER_LANGUAGE ?>	 -->
                    Language / 语言
                            	<img src="img/dropdown.png" class="dropdown-img hover1a" alt="<?php echo _HEADER_LANGUAGE ?>" title="<?php echo _HEADER_LANGUAGE ?>">
                                <img src="img/dropdown2.png" class="dropdown-img hover1b" alt="<?php echo _HEADER_LANGUAGE ?>" title="<?php echo _HEADER_LANGUAGE ?>">

                                <!-- <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->
                	
                	<div class="dropdown-content yellow-dropdown-content">
                        <!-- <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p> -->

                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a">中文</a></p>
                	</div>
                </div>  
				<!-- <a class="open-signup menu-padding menu-item menu-a">Sign Up</a>
                <a class="open-login menu-padding menu-item menu-a">Login</a> -->
                <a class="open-signup menu-padding menu-item menu-a"><?php echo _HEADER_SIGN_UP ?></a>
                <a class="open-login menu-padding menu-item menu-a"><?php echo _HEADER_LOGIN ?></a>
					
                    
                    <div id="dl-menu" class="dl-menuwrapper">
						<button class="dl-trigger">Open Menu</button>
						<ul class="dl-menu">
								  <li><a href="<?php $link ?>?lang=en">English</a></li>
                                  <li><a href="<?php $link ?>?lang=ch">中文</a></li>
                                  <li><a class="open-signup"><?php echo _HEADER_SIGN_UP ?></a></li>
                                  <li><a class="open-login"><?php echo _HEADER_LOGIN ?></a></li>                                                                                                    
   
						</ul>
					</div><!-- /dl-menuwrapper -->                
                
                                       	
            </div>
        </div>

</header>
