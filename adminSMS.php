<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Message.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

// $soundfile = "file.mp3";
$soundfile = "dingdong.mp3";
$messageValue = getMessage($conn," WHERE admin_status = ? ",array("admin_status"),array('GET'),"s");

if($messageValue)
{  
$totalMessageValue = count($messageValue);
// echo $totalMessageValue;
?>
    <div class="blue-button text-center float-left admin-msg-btn opacity-zero">
        <?php 
            echo _USERDASHBOARD_CUSTOMER_SERVICE ;
            echo "<embed src =\"$soundfile\" hidden=\"true\" autostart=\"true\"></embed>";
        ?>
    </div>
<?php
}
else
{   
    $totalMessageValue = 0;   
    ?>
        <div class="blue-button text-center float-left admin-msg-btn opacity-zero">
            <?php echo _USERDASHBOARD_CUSTOMER_SERVICE ?>
        </div>
    <?php
}

$conn->close();
?>