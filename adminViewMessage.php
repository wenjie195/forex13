<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

// $userSMS = getUser($conn," WHERE user_type = '1' ");

$userSMSDetails = getMessage($conn,"ORDER BY date_created DESC");

$messageValue = getMessage($conn," WHERE admin_status = ? ",array("admin_status"),array('GET'),"s");
// $userSMSDetails = $messageRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminViewMessage.php" />
    <meta property="og:title" content="View All Message | De Xin Guo Ji 德鑫国际" />

    <title>View All Message | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminViewMessage.php" />
    <?php include 'css.php'; ?>

</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
    <?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2">
    <!-- <h1 class="menu-distance h1-title white-text text-center">View All Message</h1> -->

    <h1 class="menu-distance h1-title white-text text-center" ><span class="blue-link">View New Message</span> |  <a href="adminViewOldMessage.php">View Old Message</a> </h1>

    <div class="width100 overflow blue-opa-bg padding-box radius-box">
    <table class="table-width data-table message-table">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th>SENDER</th>
                            <th>VIEW MESSAGE</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $conn = connDB();
                        //way to get message table uid
                        // $userSMSDetails = getMessage($conn);

                        //use message table uid to get user table uid
                        // $user = getUser($conn," WHERE user_type = '1' AND message != '' ");
                        $user = getUser($conn," WHERE user_type = '1' AND message = 'YES' ");
                        if($user)
                        {   
                            for($cnt = 0;$cnt < count($user) ;$cnt++)
                            {
                            ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $user[$cnt]->getUsername();?></td>      
                                     
                                <td>
                                    <form action="adminReplyMessage.php" method="POST">
                                        <button class="clean hover1 blue-button smaller-font" type="submit" name="message_uid" value="<?php echo $user[$cnt]->getUid();?>">
                                            REPLY
                                        </button>
                                    </form>
                                </td>     
                            </tr>
                            <?php
                            }
                            ?>
                        <?php
                        }

                        ?>
                    </tbody>

                </table>
    </div>
    </div>

    <div id="divAdminViewSMS">
    </div>

</div>
<?php include 'js.php'; ?>
</body>

<script type="text/javascript">
$(document).ready(function()
{
    $("#divAdminViewSMS").load("adminViewSMS.php");
setInterval(function()
{
    $("#divAdminViewSMS").load("adminViewSMS.php");
}, 5000);
});
</script>

</html>