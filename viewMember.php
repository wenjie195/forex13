<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$userRows = getUser($conn," WHERE user_type =1 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<!-- <?php //include 'meta.php'; ?> -->
    <meta property="og:url" content="https://dxforextrade88.com/viewMember.php" />
    <meta property="og:title" content="View All User | De Xin Guo Ji 德鑫国际" />
    <title>View All User | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/viewMember.php" />
	<!-- <?php //include 'css.php'; ?> -->
</head>
<body class="body">

<!-- <?php //echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php //include 'adminSidebar.php'; ?> -->

<div class="next-to-sidebar">

    <h1 class="h1-title">All Telemarketer List</h1>
    
    <div class="clear"></div>

    <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="shipping-table">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <!-- <th>MEMBER ID</th> -->
                            <th>NAME</th>
                            <!-- <th>FULLNAME</th> -->
                            <th>EMAIL</th>
                            <th>PHONE</th>
                            <th>ADDRESS</th>
                            <th>JOINED DATE</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php

                        if($userRows)
                        {   
                            for($cnt = 0;$cnt < count($userRows) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <!-- <td><?php //echo $userRows[$cnt]->getId();?></td> -->
                                <!-- <td><?php //echo $userRows[$cnt]->getUid();?></td> -->
                                <!-- <td><?php //echo $userRows[$cnt]->getUsername();?></td> -->
                                <td><?php echo $userRows[$cnt]->getFullName();?></td>
                                <td><?php echo $userRows[$cnt]->getEmail();?></td>
                                <td><?php echo $userRows[$cnt]->getPhoneNo();?></td>
                                <td><?php echo $userRows[$cnt]->getAddress();?></td>
                                <td><?php echo $userRows[$cnt]->getDateCreated();?></td>
                            <?php
                            }?>
                            </tr>
                        <?php
                        }

                        ?>
                    </tbody>

                </table>
            </div>
    </div>
</div>

<style>
.telemarketer-li{
	color:#bf1b37;
	background-color:white;}
.telemarketer-li .hover1a{
	display:none;}
.telemarketer-li .hover1b{
	display:block;}
</style>

<!-- <?php //include 'js.php'; ?> -->

</body>
</html>