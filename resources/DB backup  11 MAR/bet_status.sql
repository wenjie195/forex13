-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 11, 2020 at 05:34 PM
-- Server version: 5.7.29
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dxforext_forex_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `bet_status`
--

CREATE TABLE `bet_status` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `current_credit` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `bet_type` varchar(255) DEFAULT NULL,
  `start_rate` varchar(255) DEFAULT NULL,
  `end_rate` varchar(255) DEFAULT NULL,
  `timeline` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `result_edited` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'ORI',
  `status_edited` varchar(255) DEFAULT NULL,
  `edit_by` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bet_status`
--

INSERT INTO `bet_status` (`id`, `trade_uid`, `uid`, `username`, `current_credit`, `amount`, `currency`, `bet_type`, `start_rate`, `end_rate`, `timeline`, `result`, `result_edited`, `status`, `status_edited`, `edit_by`, `date_created`, `date_updated`) VALUES
(42, '4856ed62a9d1c7f136b3872c0d00f570', '5c5fdb4c0ef833d562434e0cdacd952d', 'yln88', '9200', 800, 'GBP/USD', 'BUY', '1.3035', '1.3034', 60, '', 'LOSE', 'ORI', NULL, NULL, '2020-03-10 07:35:17', '2020-03-10 07:36:15'),
(43, '6b5eea4168daa601d478ce3448114e3a', 'ca6a4bf533eae83823d60b8b9c877b90', 'wby123456', '9200', 800, 'GBP/USD', 'SELL', '1.3035', '', 60, '', '', 'ORI', NULL, NULL, '2020-03-10 07:35:19', '2020-03-10 07:35:19'),
(44, 'c8a94163b84ddfa383938daeace5a1b9', '5c5fdb4c0ef833d562434e0cdacd952d', 'yln88', '8000', 1200, 'GBP/USD', 'SELL', '1.3034', '', 60, '', '', 'ORI', NULL, NULL, '2020-03-10 07:36:36', '2020-03-10 07:36:36'),
(45, 'd7fcc1787be536857d694a15d5a64bb5', '5c5fdb4c0ef833d562434e0cdacd952d', 'yln88', '16000', 2000, 'GBP/USD', 'BUY', '1.3041', '1.3048', 60, '', 'WIN', 'ORI', NULL, NULL, '2020-03-10 07:39:56', '2020-03-10 07:40:39'),
(46, '8c12b01d750d5af422a881fae38a5d2a', 'ca6a4bf533eae83823d60b8b9c877b90', 'wby123456', '8000', 1200, 'GBP/USD', 'SELL', '1.3053', '', 60, '', '', 'ORI', NULL, NULL, '2020-03-10 07:42:11', '2020-03-10 07:42:11'),
(47, 'bea27354f1274dec26c7c7c8cb624ed9', '5c5fdb4c0ef833d562434e0cdacd952d', 'yln88', '18800', 1200, 'GBP/USD', 'BUY', '1.3052', '1.3047', 60, '', 'LOSE', 'ORI', NULL, NULL, '2020-03-10 07:42:19', '2020-03-10 07:42:57'),
(48, 'eabce97f4495a7f8e65b5ecdc027aaf5', 'ca6a4bf533eae83823d60b8b9c877b90', 'wby123456', '6500', 1500, 'GBP/USD', 'SELL', '1.3049', '', 60, '', '', 'ORI', NULL, NULL, '2020-03-10 07:43:52', '2020-03-10 07:43:52'),
(49, '2c2805adbd30115dd4fab1a4fbfc5184', '5c5fdb4c0ef833d562434e0cdacd952d', 'yln88', '17300', 1500, 'GBP/USD', 'BUY', '1.3048', '1.3048', 60, '', 'WIN', 'ORI', NULL, NULL, '2020-03-10 07:43:53', '2020-03-10 07:43:53'),
(50, 'b6b31c2c0ebec194e5ef6ce2cc852221', '5c5fdb4c0ef833d562434e0cdacd952d', 'yln88', '15100', 2200, 'GBP/USD', 'BUY', '1.3052', '1.3049', 60, '', 'LOSE', 'ORI', NULL, NULL, '2020-03-10 07:45:01', '2020-03-10 07:45:27'),
(51, 'af0488b5101d3efe9926e44e3bccb952', 'ca6a4bf533eae83823d60b8b9c877b90', 'wby123456', '9700', 2200, 'GBP/USD', 'SELL', '1.3051', '', 60, '', '', 'ORI', NULL, NULL, '2020-03-10 07:45:04', '2020-03-10 07:45:04'),
(52, 'a8af1bfdc0a0b36d985a1b50c34ff975', 'ca6a4bf533eae83823d60b8b9c877b90', 'wby123456', '11780', 2320, 'GBP/USD', 'SELL', '1.3054', '', 60, '', '', 'ORI', NULL, NULL, '2020-03-10 07:46:46', '2020-03-10 07:46:46'),
(53, '801d4f3d90b30cdcf046fb50c58a0098', '5c5fdb4c0ef833d562434e0cdacd952d', 'yln88', '15780', 2320, 'GBP/USD', 'BUY', '1.3054', '1.3055', 60, '', 'WIN', 'ORI', NULL, NULL, '2020-03-10 07:46:46', '2020-03-10 07:47:21'),
(54, 'aa13d17239441db67d37e42915c42944', '5c5fdb4c0ef833d562434e0cdacd952d', 'yln88', '12980', 2800, 'GBP/USD', 'BUY', '1.3054', '1.3054', 60, '', 'WIN', 'ORI', NULL, NULL, '2020-03-10 07:47:43', '2020-03-10 07:47:44'),
(55, '882bef69d24249d58f50e006c9d9ec5e', 'ca6a4bf533eae83823d60b8b9c877b90', 'wby123456', '13620', 2800, 'GBP/USD', 'SELL', '1.3054', '', 60, '', '', 'ORI', NULL, NULL, '2020-03-10 07:47:45', '2020-03-10 07:47:45'),
(56, '4a258674f79431358da2083f52e26994', '5c5fdb4c0ef833d562434e0cdacd952d', 'yln88', '21480', 3500, 'GBP/USD', 'BUY', '1.3053', '1.3056', 60, '', 'WIN', 'ORI', NULL, NULL, '2020-03-10 07:48:51', '2020-03-10 07:49:36'),
(57, '2c5e04bbb6a6edb8e5ad809f0fa78ad6', 'ca6a4bf533eae83823d60b8b9c877b90', 'wby123456', '15720', 3500, 'GBP/USD', 'SELL', '1.3053', '', 60, '', '', 'ORI', NULL, NULL, '2020-03-10 07:48:52', '2020-03-10 07:48:52'),
(58, 'bd990f45d98c4bd48e0e69544bc5ee9c', 'ca6a4bf533eae83823d60b8b9c877b90', 'wby123456', '17220', 5500, 'GBP/USD', 'SELL', '1.3056', '', 60, '', '', 'ORI', NULL, NULL, '2020-03-10 07:49:59', '2020-03-10 07:49:59'),
(59, '4347a64102fdb7a1af9537621678dc34', '5c5fdb4c0ef833d562434e0cdacd952d', 'yln88', '26220', 5500, 'GBP/USD', 'BUY', '1.3056', '1.3056', 60, '', 'WIN', 'ORI', NULL, NULL, '2020-03-10 07:50:00', '2020-03-10 07:50:00'),
(60, '6dd5089d306a3022341a8234e47a852f', '5c5fdb4c0ef833d562434e0cdacd952d', 'yln88', '31420', 6800, 'GBP/USD', 'BUY', '1.3052', '1.3053', 60, '', 'WIN', 'ORI', NULL, NULL, '2020-03-10 07:50:51', '2020-03-10 07:50:52'),
(61, 'e074e4926d5e138bc967414e4c2fe5bb', 'ca6a4bf533eae83823d60b8b9c877b90', 'wby123456', '21420', 6800, 'GBP/USD', 'SELL', '1.3053', '', 60, '', '', 'ORI', NULL, NULL, '2020-03-10 07:50:51', '2020-03-10 07:50:51'),
(62, 'de4ec1987d8a1da51083e4741ae1dbfe', 'ca6a4bf533eae83823d60b8b9c877b90', 'wby123456', '30920', 4100, 'GBP/USD', 'SELL', '1.3056', '', 60, '', '', 'ORI', NULL, NULL, '2020-03-10 07:52:13', '2020-03-10 07:52:13'),
(63, '38dff2f1dacd5c815e3b5d2c1875fa61', '5c5fdb4c0ef833d562434e0cdacd952d', 'yln88', '65520', 4100, 'GBP/USD', 'BUY', '1.3056', '1.3056', 60, '', 'WIN', 'ORI', NULL, NULL, '2020-03-10 07:52:13', '2020-03-10 07:52:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bet_status`
--
ALTER TABLE `bet_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bet_status`
--
ALTER TABLE `bet_status`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
