-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 11, 2020 at 05:34 PM
-- Server version: 5.7.29
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dxforext_forex_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `edit_record`
--

CREATE TABLE `edit_record` (
  `id` bigint(255) NOT NULL,
  `trade_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `amount` int(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `result_edited` varchar(255) DEFAULT NULL,
  `edit_by` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `edit_record`
--

INSERT INTO `edit_record` (`id`, `trade_uid`, `uid`, `amount`, `result`, `result_edited`, `edit_by`, `date_created`, `date_updated`) VALUES
(1, '3b9a03fcadcb2c5700f845a9b896cfa6', '8af86e839995eb50aab32848affd2608', 100, '', 'LOSE', 'admin', '2020-02-27 08:32:19', '2020-02-27 08:32:19'),
(2, 'cd24a030d0d1ad33c5ffb7abebb53579', '8af86e839995eb50aab32848affd2608', 100, '', 'LOSE', 'admin', '2020-02-27 08:38:35', '2020-02-27 08:38:35'),
(3, 'e33a8f00f1058b44c2bc2828f3dd8463', '8af86e839995eb50aab32848affd2608', 100, '', 'LOSE', 'admin', '2020-02-27 08:41:04', '2020-02-27 08:41:04'),
(4, 'ebd2df5e26fd09ce970e05632853e880', '8af86e839995eb50aab32848affd2608', 300, '', 'LOSE', 'admin', '2020-02-27 08:44:01', '2020-02-27 08:44:01'),
(5, '960a66cbc78564ec9a4cd322295736f5', '8af86e839995eb50aab32848affd2608', 100, '', 'LOSE', 'admin', '2020-02-27 08:45:14', '2020-02-27 08:45:14'),
(6, 'f6b7945b8c8446fddfa87f4dbfbcc636', 'ad5b56f368d7456e60abf9d7852f7088', 3000, '', 'LOSE', 'admin', '2020-02-28 03:44:51', '2020-02-28 03:44:51'),
(7, '89e48b4fc508c0fb7ab9447be6659029', 'ad5b56f368d7456e60abf9d7852f7088', 100, '', 'LOSE', 'admin', '2020-02-28 03:47:00', '2020-02-28 03:47:00'),
(8, '89e48b4fc508c0fb7ab9447be6659029', 'ad5b56f368d7456e60abf9d7852f7088', 100, '', 'LOSE', 'admin', '2020-02-28 03:47:00', '2020-02-28 03:47:00'),
(9, '095b42883752b6b6f77872ce01435e68', 'ad5b56f368d7456e60abf9d7852f7088', 100, '', 'LOSE', 'admin', '2020-02-28 03:47:27', '2020-02-28 03:47:27'),
(10, 'ac5c6ba26d05f021326ad213964f68ba', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'LOSE', 'WIN', 'admin', '2020-02-28 04:03:52', '2020-02-28 04:03:52'),
(11, 'ff19fc09635cfb7ed2fc59bea999382b', 'ad5b56f368d7456e60abf9d7852f7088', 100, '', 'LOSE', 'admin', '2020-02-28 04:05:04', '2020-02-28 04:05:04'),
(12, 'f22d60a1d2282b7cba8b85d473b8c1c5', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'WIN', 'LOSE', 'admin', '2020-02-28 04:05:26', '2020-02-28 04:05:26'),
(13, 'f4a03cdd936cca52f084d51514b6e549', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'WIN', 'LOSE', 'admin', '2020-02-28 04:06:08', '2020-02-28 04:06:08'),
(14, 'f4a03cdd936cca52f084d51514b6e549', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'WIN', 'LOSE', 'admin', '2020-02-28 04:06:08', '2020-02-28 04:06:08'),
(15, '674d744bf33d680fcf58b2777c7d214e', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'WIN', 'LOSE', 'admin', '2020-02-28 04:06:24', '2020-02-28 04:06:24'),
(16, '666cd7b19d8b6eb25c68f545feeebb35', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'WIN', 'LOSE', 'admin', '2020-02-28 04:12:15', '2020-02-28 04:12:15'),
(17, '5a50732ebee283738a297d97267e23d0', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'LOSE', 'WIN', 'admin', '2020-02-28 04:12:20', '2020-02-28 04:12:20'),
(18, '0b750e78a3bf2b644ac0732266b1287a', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'LOSE', 'WIN', 'admin', '2020-02-28 04:12:37', '2020-02-28 04:12:37'),
(19, '0b750e78a3bf2b644ac0732266b1287a', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'LOSE', 'WIN', 'admin', '2020-02-28 04:12:37', '2020-02-28 04:12:37'),
(20, 'cdbdbf360ba0fe69720091ff8d1c7aad', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'WIN', 'LOSE', 'admin', '2020-02-28 04:13:21', '2020-02-28 04:13:21'),
(21, 'a3c886478c1782bfd3ccc9ba240014d7', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'LOSE', 'WIN', 'admin', '2020-02-28 04:15:18', '2020-02-28 04:15:18'),
(22, 'a0f458cf878ea7edea6ad2e4b61abf56', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'LOSE', 'WIN', 'admin', '2020-02-28 04:21:43', '2020-02-28 04:21:43'),
(23, '540088477c9eb1a822427c39a9a481a7', 'ad5b56f368d7456e60abf9d7852f7088', 2000, 'LOSE', 'WIN', 'admin', '2020-02-28 04:33:08', '2020-02-28 04:33:08'),
(24, '540088477c9eb1a822427c39a9a481a7', 'ad5b56f368d7456e60abf9d7852f7088', 2000, 'LOSE', 'WIN', 'admin', '2020-02-28 04:33:08', '2020-02-28 04:33:08'),
(25, 'eb492a11e2fec29e309476310fa5ab64', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, 'LOSE', 'WIN', 'admin', '2020-02-28 10:11:11', '2020-02-28 10:11:11'),
(26, '195792c83687b8fc4d43de2f239805b1', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, 'LOSE', 'WIN', 'admin', '2020-02-28 10:11:22', '2020-02-28 10:11:22'),
(27, '5c5fba008d44cafe37acb94b5d60262f', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, 'LOSE', 'WIN', 'admin', '2020-02-28 10:11:35', '2020-02-28 10:11:35'),
(28, '9a710c546eaa5dba62c65aea81c69a8b', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, 'LOSE', 'WIN', 'admin', '2020-02-28 10:11:52', '2020-02-28 10:11:52'),
(29, '87c723e703ceec1538e241749d8c4d22', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, 'WIN', 'LOSE', 'admin', '2020-02-28 10:12:12', '2020-02-28 10:12:12'),
(30, 'e4fd1d5cfa8c07a8e109e072a605e0aa', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, 'LOSE', 'WIN', 'admin', '2020-02-28 10:14:05', '2020-02-28 10:14:05'),
(31, 'e4fd1d5cfa8c07a8e109e072a605e0aa', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, 'LOSE', 'WIN', 'admin', '2020-02-28 10:14:05', '2020-02-28 10:14:05'),
(32, 'fbf310e7f18b4a1c58259a79a5791ff0', 'ad5b56f368d7456e60abf9d7852f7088', 100, 'LOSE', 'WIN', 'admin', '2020-02-28 10:15:35', '2020-02-28 10:15:35'),
(33, '442ee3398f67dba38f4d72650d5e6030', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, 'LOSE', 'WIN', 'admin', '2020-02-28 10:15:51', '2020-02-28 10:15:51'),
(34, '442ee3398f67dba38f4d72650d5e6030', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, 'LOSE', 'WIN', 'admin', '2020-02-28 10:15:51', '2020-02-28 10:15:51'),
(35, '35d76437fce1a9076a3eac21a9c3202f', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, 'LOSE', 'WIN', 'admin', '2020-02-28 10:18:34', '2020-02-28 10:18:34'),
(36, 'e7ed5b11f1cdf1f9aa77074ec08bd2b2', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, 'LOSE', 'WIN', 'admin', '2020-02-28 10:20:49', '2020-02-28 10:20:49'),
(37, 'cd0fbe2d540f20cf4772970f61bf2ad6', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, 'WIN', 'LOSE', 'admin', '2020-02-28 10:21:09', '2020-02-28 10:21:09'),
(38, 'cd0fbe2d540f20cf4772970f61bf2ad6', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, 'WIN', 'LOSE', 'admin', '2020-02-28 10:21:09', '2020-02-28 10:21:09'),
(39, '8dba6f74eb43d535f540d6b7203e1b32', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, 'WIN', 'LOSE', 'admin', '2020-02-28 10:24:54', '2020-02-28 10:24:54'),
(40, '5fd9d5f670b0e9367c2c76510da2cfc8', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 500, 'LOSE', 'WIN', 'admin', '2020-02-28 10:25:43', '2020-02-28 10:25:43'),
(41, '0111fa27816f7485006a1c84596f84a3', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 5000, '', 'LOSE', 'admin', '2020-02-28 10:27:07', '2020-02-28 10:27:07'),
(42, '3d10e108393aae8fca343adff570ab89', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 100, '', 'LOSE', 'admin', '2020-02-28 10:27:15', '2020-02-28 10:27:15'),
(43, '3c1c2c4f1be8b76c3b98279d48a5d001', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 2000, '', 'LOSE', 'admin', '2020-02-28 10:27:47', '2020-02-28 10:27:47'),
(44, 'd4fa0328a7addb493c9db82c18a90466', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 2000, '', 'LOSE', 'admin', '2020-02-28 10:28:26', '2020-02-28 10:28:26'),
(45, '86d010133965a0c2a74664446458271c', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 500, '', 'LOSE', 'admin', '2020-02-28 10:29:37', '2020-02-28 10:29:37'),
(46, 'dfb6fb84172b637af709d4aa54a389d8', 'b7e3f1924c9e1bcf04830e7d4ebcc57c', 500, '', 'LOSE', 'admin', '2020-02-28 10:30:37', '2020-02-28 10:30:37'),
(47, 'a88b80722c905cd7b96aeae70781de20', 'ad5b56f368d7456e60abf9d7852f7088', 1000, '', 'LOSE', 'admin', '2020-03-01 07:49:04', '2020-03-01 07:49:04'),
(48, 'd04129219ff7cdfe639ba073a529c149', 'ad5b56f368d7456e60abf9d7852f7088', 2000, '', 'LOSE', 'admin', '2020-03-01 07:49:38', '2020-03-01 07:49:38'),
(49, '6699cd8592077e22a52502ffb5d798b3', 'ad5b56f368d7456e60abf9d7852f7088', 300, '', 'LOSE', 'admin', '2020-03-01 07:52:16', '2020-03-01 07:52:16'),
(50, '725833c2fc980176bf7305a90d7c0ff8', 'ad5b56f368d7456e60abf9d7852f7088', 2000, '', 'LOSE', 'admin', '2020-03-01 07:54:56', '2020-03-01 07:54:56'),
(51, 'd617d0291120feb8cafb048648d66a1b', 'ad5b56f368d7456e60abf9d7852f7088', 30000, '', 'LOSE', 'admin', '2020-03-01 07:55:21', '2020-03-01 07:55:21'),
(52, '8f31be6706ca246c454b2930f984f7d9', 'ad5b56f368d7456e60abf9d7852f7088', 2000, '', 'LOSE', 'admin', '2020-03-01 08:09:12', '2020-03-01 08:09:12'),
(53, 'f820b6d6d62bc4fe0c1a38e2e210295a', 'ad5b56f368d7456e60abf9d7852f7088', 2000, '', 'LOSE', 'admin', '2020-03-01 08:09:46', '2020-03-01 08:09:46'),
(54, '9d053a128b53d0d60da7486394fb236f', 'ad5b56f368d7456e60abf9d7852f7088', 5000, '', 'LOSE', 'admin', '2020-03-01 08:23:16', '2020-03-01 08:23:16'),
(55, '33cec3c99cd0640d047fa4d0dd217522', 'ad5b56f368d7456e60abf9d7852f7088', 5000, '', 'LOSE', 'admin', '2020-03-01 08:50:23', '2020-03-01 08:50:23'),
(56, 'b05922012d52048867ed3bc69169e77f', 'ad5b56f368d7456e60abf9d7852f7088', 5000, '', 'LOSE', 'user1', '2020-03-01 10:24:14', '2020-03-01 10:24:14'),
(57, 'b05922012d52048867ed3bc69169e77f', 'ad5b56f368d7456e60abf9d7852f7088', 5000, '', 'LOSE', 'user1', '2020-03-01 10:24:43', '2020-03-01 10:24:43'),
(58, 'dcbfd9763758b3bf8c7585083d0480b2', 'ad5b56f368d7456e60abf9d7852f7088', 5000, '', 'LOSE', 'user1', '2020-03-04 06:59:11', '2020-03-04 06:59:11'),
(59, 'dcbfd9763758b3bf8c7585083d0480b2', 'ad5b56f368d7456e60abf9d7852f7088', 5000, '', 'LOSE', 'user1', '2020-03-04 06:59:11', '2020-03-04 06:59:11'),
(60, 'fd5c621d7b0a9e1687e5efec800bed65', 'ad5b56f368d7456e60abf9d7852f7088', 5000, '', 'LOSE', 'admin', '2020-03-04 09:30:14', '2020-03-04 09:30:14'),
(61, 'fd5c621d7b0a9e1687e5efec800bed65', 'ad5b56f368d7456e60abf9d7852f7088', 5000, '', 'LOSE', 'admin', '2020-03-04 09:30:14', '2020-03-04 09:30:14'),
(62, 'fd5c621d7b0a9e1687e5efec800bed65', 'ad5b56f368d7456e60abf9d7852f7088', 5000, '', 'LOSE', 'admin', '2020-03-04 09:30:16', '2020-03-04 09:30:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `edit_record`
--
ALTER TABLE `edit_record`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `edit_record`
--
ALTER TABLE `edit_record`
  MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
