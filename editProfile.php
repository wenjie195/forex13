<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/editProfile.php" />
    <meta property="og:title" content="Edit Profile | De Xin Guo Ji 德鑫国际" />
    <title>Edit Profile | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/editProfile.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAfterLogin.php'; ?>
    <h1 class="menu-distance h1-title white-text text-center"><?php echo _HEADER_EDIT_PROFILE ?></h1>
    <div class="width100 overflow blue-opa-bg padding-box radius-box smaller-box">
        <!-- <form> -->
        <form action="utilities/editProfileFunction.php" method="POST">
        <div class="dual-input-div">
            <p class="input-top-text"><?php echo _JS_FULLNAME ?></p>
            <input class="clean de-input p6" type="text" placeholder="<?php echo _JS_FULLNAME ?>" value="<?php echo $userDetails->getFullName();?>" id="edit_fullname" name="edit_fullname">
		</div>
		<div class="dual-input-div second-dual-input">
            <p class="input-top-text"><?php echo _JS_USERNAME ?></p>
            <input class="clean de-input p6" type="text" placeholder="<?php echo _JS_USERNAME ?>" value="<?php echo $userDetails->getUsername();?>" id="edit_username" name="edit_username">
		</div>
            <div class="clear"></div>

            <div class="dual-input-div">
                <p class="input-top-text"><?php echo _JS_EMAIL ?></p>
                <input class="clean de-input p6" type="email" placeholder="<?php echo _JS_EMAIL ?>" value="<?php echo $userDetails->getEmail();?>" id="edit_email" name="edit_email">
          
            </div>
            <div class="dual-input-div second-dual-input">
                <p class="input-top-text"><?php echo _JS_PHONE ?></p>
                <input class="clean de-input p6" type="text" placeholder="<?php echo _JS_PHONE ?>" value="<?php echo $userDetails->getPhoneNo();?>" id="edit_phone" name="edit_phone">
            </div>  

            <div class="clear"></div>


            <div class="dual-input-div">
                <p class="input-top-text"><?php echo _JS_CURRENT_PASSWORD ?></p>
                <input class="clean de-input p6" type="password" placeholder="<?php echo _JS_CURRENT_PASSWORD ?>" id="editPassword_current" name="editPassword_current">
            
            </div>
            <div class="dual-input-div second-dual-input">
                <p class="input-top-text"><?php echo _JS_NEW_PASSWORD ?></p>
                <input class="clean de-input p6" type="text" placeholder="<?php echo _JS_NEW_PASSWORD ?>" id="editPassword_new" name="editPassword_new" >
            </div>  
			<div class="clear"></div>
            <div class="dual-input-div">
                <p class="input-top-text"><?php echo _JS_RETYPE_PASSWORD ?></p>
                <input class="clean de-input p6" type="password" placeholder="<?php echo _JS_RETYPE_PASSWORD ?>" id="editPassword_reenter" name="editPassword_reenter">
            </div>

            
			<div class="clear"></div>
            <button class="clean blue-button mid-button-width small-distance small-distance-bottom"><?php echo _JS_SUBMIT ?></button>

            <div class="clear"></div>
        </form>
	</div>
</div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "此用户的户口已被激活！";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "此用户的户口已被停用！";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "无法更新此用户的户口状体！";
        }  
        else if($_GET['type'] == 3)
        {
            $messageType = "出了点小状况，请稍后再试！";
        }       
        echo '
        <script>
            putNoticeJavascript("通告 !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;

    }
}
?>

</body>
</html>