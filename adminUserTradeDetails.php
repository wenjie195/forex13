<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/Deposit.php';
require_once dirname(__FILE__) . '/classes/ReferralHistory.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$totalProfit = getBetstatus($conn, "WHERE uid = ? AND result_edited = 'WIN' ", array("uid"),array($_POST['useruid_tradedetails']), "s");
$totalLose = getBetstatus($conn, "WHERE uid = ? AND result_edited = 'LOSE' ", array("uid"),array($_POST['useruid_tradedetails']), "s");


if($totalProfit)
{
    $totalProfitAmount = 0;
    for ($cnt=0; $cnt <count($totalProfit) ; $cnt++)
    {
        $totalProfitAmount += $totalProfit[$cnt]->getAmount();
    }
}
else
{   $totalProfitAmount = 0 ;    }

if($totalLose)
{
    $totalLoseAmount = 0;
    for ($cnt=0; $cnt <count($totalLose) ; $cnt++)
    {
        $totalLoseAmount += $totalLose[$cnt]->getAmount();
    }
}
else{   $totalLoseAmount = 0 ;  }

$nettGainLose = $totalProfitAmount - $totalLoseAmount;
                                
$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminUserTradeDetails.php" />
    <meta property="og:title" content="User Details | De Xin Guo Ji 德鑫国际" />
    <title>User Details | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminUserTradeDetails.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<div class="dark-bg overflow same-padding">
<?php include 'headerAdmin.php'; ?>
<?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2">
        <h1 class="menu-distance h1-title white-text text-center"><?php echo _AUD_CUSDETAILA ?></h1>
        <div class="width100 overflow blue-opa-bg padding-box radius-box">
        <table class="table-width data-table message-table">
                    <thead>
                        <tr>
                            <th><?php echo _JS_USERNAME ?></th>
                            <!-- <th>Amount</th> -->
                            
                            <th><?php echo _AUD_ACCOUNTNO ?></th>
                            <th><?php echo _AUD_AMOUNT ?></th>

                            <th><?php echo _AUTD_NOOFWIN ?></th>
                            <th><?php echo _AUTD_NOOFLOSE ?></th>
                            <th><?php echo _AUTD_NEETGL ?></th>

                            <!-- <th>Upline Name</th> -->

                            <!-- <th>No. of Win</th>
                            <th>No. of Lose</th>
                            <th>Nett Gain / Lose</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $conn = connDB();
                        $asd = $_POST['useruid_tradedetails'];
                        $userRows = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['useruid_tradedetails']),"s");

                        $userUpline = getReferralHistory($conn,"WHERE referral_id = ? ", array("referral_id") ,array($_POST['useruid_tradedetails']),"s");
                        if($userUpline != null)
                        {  
                            $uplineUID = $userUpline[0];  
                            $asd = $uplineUID->getReferrerId();
                            // $uplineUID->getReferralName();
                            // $uplineUID->getReferralId();

                            $uplineDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($asd),"s");
                            $uplineRows = $uplineDetails[0];  
                            $uplineUsername = $uplineRows->getUsername();
                        }
                        else
                        {   }

                        if($userRows != null)
                        {   
                            for($cnt = 0;$cnt < count($userRows) ;$cnt++)
                            {?>
                            <tr>
                                <td><?php echo $userRows[$cnt]->getUsername();?></td>
                                <td><?php echo $userRows[$cnt]->getBankAccountNo();?></td>
                                <td><?php echo $userRows[$cnt]->getCredit();?></td>
                                <td>
                                    <?php
                                    if($totalProfit)
                                    {   
                                        $totalProfitNumCount = count($totalProfit);
                                    }
                                    else
                                    {   $totalProfitNumCount = 0;   }
                                    ?>
                                    <?php echo $totalProfitNumCount;?>
                                </td>
                                <td>
                                    <?php
                                    if($totalLose)
                                    {   
                                        $totalLoseNumCount = count($totalLose);
                                    }
                                    else
                                    {   $totalLoseNumCount = 0;   }
                                    ?>
                                    <?php echo $totalLoseNumCount;?>
                                </td>
                                <td>
                                    <?php echo $nettGainLose?>
                                </td>

                                
                                
                            <?php
                            }?>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
        </div>
        
        <div class="width100 overflow tab-divdiv tab">
            <!-- <button class="tablinks clean" onclick="openTab(event, 'Win')">Win</button>
            <button class="tablinks clean" onclick="openTab(event, 'Lose')">Lose</button>
            <button class="tablinks clean" onclick="openTab(event, 'Topup')">Top Up</button>
            <button class="tablinks clean" onclick="openTab(event, 'Withdrawal')">Withdrawal</button> -->
            <button class="tablinks clean" onclick="openTab(event, 'Win')"><?php echo _AUTD_WIN ?></button>
            <button class="tablinks clean" onclick="openTab(event, 'Lose')"><?php echo _AUTD_LOSE ?></button>
            <button class="tablinks clean" onclick="openTab(event, 'Topup')"><?php echo _AUTD_TOPUP ?></button>
            <button class="tablinks clean" onclick="openTab(event, 'Withdrawal')"><?php echo _AUTD_WITHDRAWAL ?></button>
        </div>

        <!-- <div id="Win" class="tabcontent activetab small-distance block"></div> -->

        <div id="Win" class="tabcontent activetab small-distance block">
            <table class="table-width data-table">
                <thead>
                    <tr>
                        <!-- <th><b>Currency Pair</b></th>
                        <th><b>Amount</b></th>
                        <th><b>Timeframe</b></th>
                        <th><b>Date</b></th> -->
                        <th><b><?php echo _ADMINTOTALPROFITLOSS_CURRENCY_PAIR ?></b></th>
                        <th><b><?php echo _AUD_AMOUNT ?></b></th>
                        <th><b><?php echo _ADMINTOTALPROFITLOSS_TIMEFRAME ?></b></th>
                        <th><b><?php echo _AUD_DATE ?></b></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $conn = connDB();
                    $depositArray = getBetstatus($conn,"WHERE uid = ? AND result_edited = 'WIN' ORDER BY date_created DESC", array("uid") ,array($_POST['useruid_tradedetails']),"s");
                    if($depositArray != null)
                    {   
                        for($cnt = 0;$cnt < count($depositArray) ;$cnt++)
                        {
                        ?>
                        <tr>
                            <!-- <td><?php //echo ($cnt+1)?></td> -->
                            <td><?php echo $depositArray[$cnt]->getCurrency();?></td>
                            <td class="red-text"><?php echo $depositArray[$cnt]->getAmount();?></td>
                            <td><?php echo $depositArray[$cnt]->getTimeline();?> sec</td>
                            <td><?php echo $depositArray[$cnt]->getDateCreated();?></td>
                        <?php
                        }
                        ?>
                        </tr>
                    <?php
                    }
                    ?>   
                </tbody>
            </table>
        </div>

        <div id="Lose" class="tabcontent small-distance">
            <table class="table-width data-table">
                <thead>
                    <tr>
                        <!-- <th><b>Currency Pair</b></th>
                        <th><b>Amount</b></th>
                        <th><b>Timeframe</b></th>
                        <th><b>Date</b></th> -->
                        <th><b><?php echo _ADMINTOTALPROFITLOSS_CURRENCY_PAIR ?></b></th>
                        <th><b><?php echo _AUD_AMOUNT ?></b></th>
                        <th><b><?php echo _ADMINTOTALPROFITLOSS_TIMEFRAME ?></b></th>
                        <th><b><?php echo _AUD_DATE ?></b></th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $conn = connDB();
                    $betResultLose = getBetstatus($conn,"WHERE uid = ? AND result_edited = 'LOSE' ORDER BY date_created DESC", array("uid") ,array($_POST['useruid_tradedetails']),"s");
                    if($betResultLose != null)
                    {   
                        for($cnt = 0;$cnt < count($betResultLose) ;$cnt++)
                        {
                        ?>
                        <tr>
                            <td><?php echo $betResultLose[$cnt]->getCurrency();?></td>
                            <td class="green-text"><?php echo $betResultLose[$cnt]->getAmount();?></td>
                            <td><?php echo $betResultLose[$cnt]->getTimeline();?> sec</td>
                            <td><?php echo $betResultLose[$cnt]->getDateCreated();?></td>
                        <?php
                        }
                        ?>
                        </tr>
                    <?php
                    }
                    ?>   
                </tbody>
            </table>
        </div>

        <div id="Topup" class="tabcontent small-distance">
            <table class="table-width data-table">
                <thead>
                    <tr>
                        <th><?php echo _VIEWMESSAGE_NO ?></th>
                        <!-- <th>Amount (RM)</th>
                        <th>Top Up By</th>
                        <th>Date</th> -->
                        <th><?php echo _AUD_AMOUNT ?></th>
                        <th><?php echo _AUD_TOPUPBY ?></th>
                        <th><?php echo _AUD_DATE ?></th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $conn = connDB();
                    $betResultLose = getUser($conn,"WHERE uid = ?", array("uid") ,array($_POST['useruid_tradedetails']),"s");
                    $userUsername = $betResultLose[0]->getUsername();

                    $depositArray = getDeposit($conn,"WHERE username = ? ORDER BY date_created desc", array("username") ,array($userUsername),"s");
                    if($depositArray != null)
                    {   
                        for($cnt = 0;$cnt < count($depositArray) ;$cnt++)
                        {
                        ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $depositArray[$cnt]->getAmount();?></td>
                            <td><?php echo $depositArray[$cnt]->getVerifyBy();?></td>
                            <td><?php echo $depositArray[$cnt]->getDateCreated();?></td>
                        <?php
                        }
                        ?>
                        </tr>
                    <?php
                    }
                    ?>   
                </tbody>
            </table>
        </div>

        <div id="Withdrawal" class="tabcontent small-distance">
            <table class="table-width data-table">
                <thead>
                    <tr>
                        <th><?php echo _VIEWMESSAGE_NO ?></th>
                        <th class="two-white-border"><?php echo _JS_USERNAME ?></th>
                        <th class="two-white-border"><?php echo _JS_WITHDRAW_AMOUNT ?></th>
                        <th class="two-white-border"><?php echo _WITHDRAWAL_TIME ?></th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $conn = connDB();
                    $withdrawalDetails = getWithdrawal($conn,"WHERE uid = ?", array("uid") ,array($_POST['useruid_tradedetails']),"s");
                    if($withdrawalDetails != null)
                    {   
                        for($cnt = 0;$cnt < count($withdrawalDetails) ;$cnt++)
                        {?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $withdrawalDetails[$cnt]->getUsername();?></td>
                            <td><?php echo $withdrawalDetails[$cnt]->getAmount();?></td>

                            <td>
                                <?php $dateCreated = date("Y-m-d",strtotime($withdrawalDetails[$cnt]->getDateCreated())); echo $dateCreated;?>
                            </td>
                        <?php
                        }?>
                        </tr>
                    <?php
                    }

                    ?>
                </tbody>

            </table>
        </div>

    </div>
</div>

<?php include 'js.php'; ?>

</body>
</html>