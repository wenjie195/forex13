<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

// $userSMSDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created DESC",array("uid"),array($uid),"s");
// $userSMSDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created ASC",array("uid"),array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/viewMessage.php" />
    <meta property="og:title" content="View Message | De Xin Guo Ji 德鑫国际" />
    <title>View Message | De Xin Guo Ji 德鑫国际</title>
    <meta property="og:url" content="https://dxforextrade88.com/viewMessage.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">

<div class="dark-bg overflow same-padding">
	<?php include 'headerAfterLogin.php'; ?>
    <h1 class="menu-distance h1-title white-text text-center"><?php echo _VIEWMESSAGE_VIEW_ALL_MESSAGE ?></h1>
    <div class="width100 overflow blue-opa-bg padding-box radius-box chat-div-padding">
		<div class="fix-scroll-msg">       
            <div id="conversationDiv">

            </div>
        </div>

        <?php
        $conn = connDB();
        $userSMSDetails = getMessage($conn," WHERE uid = ? ORDER BY date_created ASC",array("uid"),array($uid),"s");
        if($userSMSDetails)
        {
        ?>
        
        <div class="clear"></div>
        
        <div class="fix-msg-box">
            <form action="utilities/replyMessageTwoFunction.php" method="POST">
                <!-- <input type="file" id="" name="" class="clean upload-file-input"> -->
                <input class="clean de-input left-msg" type="text" placeholder="<?php echo _VIEWMESSAGE_UR_MESSAGE ?>" id="message_details" name="message_details" required>
                <button class="clean hover1 blue-button smaller-font right-submit" type="submit" name="sent_sms">
                    <?php echo _VIEWMESSAGE_SENT2 ?>
                </button>
            </form>
            <div class="clear"></div>
            <form  action="utilities/replyMessageImageFunction.php" method="POST" enctype="multipart/form-data" class="upload-img-form">
                <!-- <input class="hidden-input" type="file" name="file" /> -->
               	<div class="file-upload-wrapper" data-text="<?php echo _VIEWMESSAGE_CHOOSE_YOUR_FILE ?>">
      				<input name="file" type="file" class="file-upload-field" required>
    			</div>
               
                <button class="clean hover1 blue-button smaller-font upload-photo ow-green-bg" type="submit" name="submit">
                    <?php echo _VIEWMESSAGE_SENT3 ?>
                </button>
            </form>
            
            
        </div>


        <?php
        }
        else
        {   ?>

            <div class="fix-msg-box">
            <form action="utilities/replyMessageTwoFunction.php" method="POST">
                <input class="clean de-input left-msg" type="text" placeholder="<?php echo _VIEWMESSAGE_UR_MESSAGE ?>" id="message_details" name="message_details" required>
                <button class="clean hover1 blue-button smaller-font right-submit" type="submit" name="sent_sms">
                    <?php echo _VIEWMESSAGE_SENT2 ?>
                </button>
            </form>
            <div class="clear"></div>
            <form  action="utilities/replyMessageImageFunction.php" method="POST" enctype="multipart/form-data" class="upload-img-form">
                <!-- <input class="hidden-input" type="file" name="file" /> -->
               	<div class="file-upload-wrapper" data-text="<?php echo _VIEWMESSAGE_CHOOSE_YOUR_FILE ?>">
      				<input name="file" type="file" class="file-upload-field" required>
    			</div>
                <button class="clean hover1 blue-button smaller-font upload-photo ow-green-bg" type="submit" name="submit">
                    c
                </button>
            </form>
        </div>
            <?php
        }
        $conn->close();
        ?>

        <form action="utilities/replyReadMessageFunction.php" method="POST">
            <button class="clean mute-button hover1 transparent-button" type="submit" name="read_sms">
            	<img src="img/mute.png" class="mute-button hover1a" alt="<?php echo _VIEWMESSAGE_MUTE ?>" title="<?php echo _VIEWMESSAGE_MUTE ?>">
                <img src="img/mute2.png" class="mute-button hover1b" alt="<?php echo _VIEWMESSAGE_MUTE ?>" title="<?php echo _VIEWMESSAGE_MUTE ?>">
            </button>
        </form>

    </div>
</div>

<style>
::-webkit-scrollbar {
  width: 3px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #15212d; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #15212d; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #15212d; 
}
</style>

<?php include 'js.php'; ?>
</body>
<script>
    $(document).ready(function(){
        $("#conversationDiv").load("conversationSMS.php");
    setInterval(function() {
        $("#conversationDiv").load("conversationSMS.php");
    }, 1000);
    });
</script>
</html>