<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/timezone.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/BetStatus.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://financialmodelingprep.com/api/v3/forex",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
		"x-rapidapi-host: currency-exchange.p.rapidapi.com",
		"x-rapidapi-key: c5875e8297msh333e08626d58b3fp1015afjsn73bce78f515f"
	),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	$exchangeRates = json_decode($response, true);
}
?>
<div  class="three-div-width border-right">
<div class="fake-header-div">
    30 <?php echo _USERDASHBOARD_SEC ?>
  </div>
<div class="clear"></div>
    <?php
    $betStatusDetails = getBetstatus($conn);
    if ($betStatusDetails) {
			for ($i=0; $i <count($betStatusDetails) ; $i++) {

        $betCurrencyVal = $betStatusDetails[$i]->getStartRate();
				$tradeUID = $betStatusDetails[$i]->getTradeUid();
				$userUID = $betStatusDetails[$i]->getUid();
        $betCurrency = $betStatusDetails[$i]->getCurrency();
        $betType = $betStatusDetails[$i]->getBetType();
				$betEditBy = $betStatusDetails[$i]->getEditBy();
				$username = $betStatusDetails[$i]->getUsername();
				$betAmount = $betStatusDetails[$i]->getAmount();
				$time = date('Y-m-d H:i:s',strtotime($betStatusDetails[$i]->getDateCreated()));
				$betTimeline = $betStatusDetails[$i]->getTimeline();
				$finishTime = date('Y-m-d H:i:s',strtotime($time."+ 30 days"));
				$finishTimeString = strtotime($finishTime);
				$currentTime = date('Y-m-d H:i:s');
				$currentTimeString = strtotime($currentTime);
				$timeLeft = $finishTimeString - $currentTimeString;
        if (!$betEditBy && $exchangeRates && $currentTime < $finishTime) {
          for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
          {
               $allCountryCurr = $exchangeRates['forexList'][$cnt]['ticker'];
               if ($allCountryCurr == $betCurrency)
               {  if ($betType == 'BUY') {
                 $rate = number_format($exchangeRates['forexList'][$cnt]['ask'], 4);
                 if ($rate < $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'LOSE';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // }
                   ?>
									 <!-- <tr style="background-color: transparent"> -->
										 <button class="clean green-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount(); ?>
										 <input type="hidden" name="result" value="WIN"></button> 
									 <!-- </tr> -->

                   <?php
                 }elseif ($rate > $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'WIN';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // }?>
									 <!-- <tr style="background-color: transparent"> -->

										 <button class="clean red-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
										 <input type="hidden" name="result" value="LOSE"></button>
									 <!-- </tr> -->

                   <?php
                 }else {
									 // if ($timeLeft < 5) {

 									 $result = 'WIN';

 									 $tableName = array();
 									 $tableValue =  array();
 									 $stringType =  "";
 									 //echo "save to database";
 									 if($result)
 									 {
 												array_push($tableName,"result_edited");
 												array_push($tableValue,$result);
 												$stringType .=  "s";
 									 }
 									 if($rate)
 										{
 												 array_push($tableName,"end_rate");
 												 array_push($tableValue,$rate);
 												 $stringType .=  "s";
 										}
 									 array_push($tableValue,$tradeUID);
 									 $stringType .=  "s";
 									 $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 									 if($deductCreditAfterTrade)
 									 {}
 									// }?>
 									<!-- <tr style="background-color: transparent"> -->

 										<button class="clean red-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
										<input type="hidden" name="result" value="LOSE"></button>
 									<!-- </tr>  -->
									<?php
                 }
               }elseif ($betType == 'SELL') {
                 $rate = number_format($exchangeRates['forexList'][$cnt]['bid'], 4);
                 if ($rate > $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'LOSE';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // } ?>
									 <!-- <tr style="background-color: transparent"> -->
									 	<button class="clean green-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
										<input type="hidden" name="result" value="WIN"></button>
									 <!-- </tr>        -->
									             <?php
                 }elseif ($rate < $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'WIN';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // }
                   ?>
									 <!-- <tr style="background-color: transparent"> -->

										 <button class="clean red-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
										 <input type="hidden" name="result" value="LOSE"></button>
									 <!-- </tr>    -->
									                <?php
                 }else {
									 // if ($timeLeft < 5) {

 									 $result = 'WIN';

 									 $tableName = array();
 									 $tableValue =  array();
 									 $stringType =  "";
 									 //echo "save to database";
 									 if($result)
 									 {
 												array_push($tableName,"result_edited");
 												array_push($tableValue,$result);
 												$stringType .=  "s";
 									 }
 									 if($rate)
 										{
 												 array_push($tableName,"end_rate");
 												 array_push($tableValue,$rate);
 												 $stringType .=  "s";
 										}
 									 array_push($tableValue,$tradeUID);
 									 $stringType .=  "s";
 									 $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 									 if($deductCreditAfterTrade)
 									 {}
 									// }?>
 									<!-- <tr style="background-color: transparent"> -->

 										<button class="clean red-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
										<input type="hidden" name="result" value="LOSE"></button></button>
 									<!-- </tr>  -->
									<?php
               }


               }
          }
        }

      }
    }}
     ?>
    <!-- <tr style="background-color: transparent"> -->

      <!-- <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php //echo $betStatusDetails[$i]->getTradeUid()?>">Lose</button> </td> -->
    <!-- </tr> -->

</div>
<div  class="three-div-width mid-three-div">
<div class="fake-header-div">
    60 <?php echo _USERDASHBOARD_SEC ?>
  </div>
<div class="clear"></div>

    <?php
    $betStatusDetails = getBetstatus($conn);
    if ($betStatusDetails) {
			for ($i=0; $i <count($betStatusDetails) ; $i++) {

        $betCurrencyVal = $betStatusDetails[$i]->getStartRate();
				$tradeUID = $betStatusDetails[$i]->getTradeUid();
				$userUID = $betStatusDetails[$i]->getUid();
        $betCurrency = $betStatusDetails[$i]->getCurrency();
        $betType = $betStatusDetails[$i]->getBetType();
				$betEditBy = $betStatusDetails[$i]->getEditBy();
				$username = $betStatusDetails[$i]->getUsername();
				$betAmount = $betStatusDetails[$i]->getAmount();
				$time = date('Y-m-d H:i:s',strtotime($betStatusDetails[$i]->getDateCreated()));
				$betTimeline = $betStatusDetails[$i]->getTimeline();
				$finishTime = date('Y-m-d H:i:s',strtotime($time."+ 30 days"));
				$finishTimeString = strtotime($finishTime);
				$currentTime = date('Y-m-d H:i:s');
				$currentTimeString = strtotime($currentTime);
				$timeLeft = $finishTimeString - $currentTimeString;
        if (!$betEditBy && $exchangeRates && $currentTime < $finishTime) {
          for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
          {
               $allCountryCurr = $exchangeRates['forexList'][$cnt]['ticker'];
               if ($allCountryCurr == $betCurrency)
               {  if ($betType == 'BUY') {
                 $rate = number_format($exchangeRates['forexList'][$cnt]['ask'], 4);
                 if ($rate < $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'LOSE';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // }
                   ?>
									 <!-- <tr style="background-color: transparent"> -->
										<button class="clean green-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount(); ?>
										 <input type="hidden" name="result" value="WIN"></button>
									 <!-- </tr> -->

                   <?php
                 }elseif ($rate > $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'WIN';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // }?>
									 <!-- <tr style="background-color: transparent"> -->

										 <button class="clean red-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
										 <input type="hidden" name="result" value="LOSE"></button>
									 <!-- </tr> -->

                   <?php
                 }else {
									 // if ($timeLeft < 5) {

 									 $result = 'WIN';

 									 $tableName = array();
 									 $tableValue =  array();
 									 $stringType =  "";
 									 //echo "save to database";
 									 if($result)
 									 {
 												array_push($tableName,"result_edited");
 												array_push($tableValue,$result);
 												$stringType .=  "s";
 									 }
 									 if($rate)
 										{
 												 array_push($tableName,"end_rate");
 												 array_push($tableValue,$rate);
 												 $stringType .=  "s";
 										}
 									 array_push($tableValue,$tradeUID);
 									 $stringType .=  "s";
 									 $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 									 if($deductCreditAfterTrade)
 									 {}
 									// }?>
 									<!-- <tr style="background-color: transparent"> -->

 										<button class="clean red-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
										<input type="hidden" name="result" value="LOSE"></button>
 									<!-- </tr>  -->
									<?php
                 }
               }elseif ($betType == 'SELL') {
                 $rate = number_format($exchangeRates['forexList'][$cnt]['bid'], 4);
                 if ($rate > $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'LOSE';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // } ?>
									 <!-- <tr style="background-color: transparent"> -->
									 	<button class="clean green-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
										<input type="hidden" name="result" value="WIN"></button>
									 <!-- </tr>   -->
									                  <?php
                 }elseif ($rate < $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'WIN';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // }
                   ?>
									 <!-- <tr style="background-color: transparent"> -->

										 <button class="clean red-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
										 <input type="hidden" name="result" value="LOSE"></button>
									 <!-- </tr>   -->
									                 <?php
                 }else {
									 // if ($timeLeft < 5) {

 									 $result = 'WIN';

 									 $tableName = array();
 									 $tableValue =  array();
 									 $stringType =  "";
 									 //echo "save to database";
 									 if($result)
 									 {
 												array_push($tableName,"result_edited");
 												array_push($tableValue,$result);
 												$stringType .=  "s";
 									 }
 									 if($rate)
 										{
 												 array_push($tableName,"end_rate");
 												 array_push($tableValue,$rate);
 												 $stringType .=  "s";
 										}
 									 array_push($tableValue,$tradeUID);
 									 $stringType .=  "s";
 									 $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 									 if($deductCreditAfterTrade)
 									 {}
 									// }?>
 									<!-- <tr style="background-color: transparent"> -->

 										<button class="clean red-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
										<input type="hidden" name="result" value="LOSE"></button>
 									<!-- </tr> -->
									 <?php
               }


               }
          }
        }

      }
    }}
     ?>
    <!-- <tr style="background-color: transparent"> -->

      <!-- <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php //echo $betStatusDetails[$i]->getTradeUid()?>">Lose</button> </td> -->
    <!-- </tr> -->

</div>

	<div  class="three-div-width border-left">
	<div class="fake-header-div">
	    180 <?php echo _USERDASHBOARD_SEC ?>
	  </div>
	<div class="clear"></div>
	  
	    <?php
	    $betStatusDetails = getBetstatus($conn);
	    if ($betStatusDetails) {
				for ($i=0; $i <count($betStatusDetails) ; $i++) {

	        $betCurrencyVal = $betStatusDetails[$i]->getStartRate();
					$tradeUID = $betStatusDetails[$i]->getTradeUid();
					$userUID = $betStatusDetails[$i]->getUid();
	        $betCurrency = $betStatusDetails[$i]->getCurrency();
	        $betType = $betStatusDetails[$i]->getBetType();
					$betEditBy = $betStatusDetails[$i]->getEditBy();
					$username = $betStatusDetails[$i]->getUsername();
					$betAmount = $betStatusDetails[$i]->getAmount();
					$time = date('Y-m-d H:i:s',strtotime($betStatusDetails[$i]->getDateCreated()));
					$betTimeline = $betStatusDetails[$i]->getTimeline();
					$finishTime = date('Y-m-d H:i:s',strtotime($time."+ 30 days"));
					$finishTimeString = strtotime($finishTime);
					$currentTime = date('Y-m-d H:i:s');
					$currentTimeString = strtotime($currentTime);
					$timeLeft = $finishTimeString - $currentTimeString;
	        if (!$betEditBy && $exchangeRates && $currentTime < $finishTime) {
	          for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
	          {
	               $allCountryCurr = $exchangeRates['forexList'][$cnt]['ticker'];
	               if ($allCountryCurr == $betCurrency)
	               {  if ($betType == 'BUY') {
	                 $rate = number_format($exchangeRates['forexList'][$cnt]['ask'], 4);
	                 if ($rate < $betCurrencyVal) {
										 // if ($timeLeft < 5) {

											$result = 'LOSE';

											$tableName = array();
	 					          $tableValue =  array();
	 					          $stringType =  "";
	 					          //echo "save to database";
	 					          if($result)
	 					          {
	 					               array_push($tableName,"result_edited");
	 					               array_push($tableValue,$result);
	 					               $stringType .=  "s";
	 					          }
											if($rate)
											 {
														array_push($tableName,"end_rate");
														array_push($tableValue,$rate);
														$stringType .=  "s";
											 }
	 					          array_push($tableValue,$tradeUID);
	 					          $stringType .=  "s";
	 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
	 					          if($deductCreditAfterTrade)
	 					          {}
										 // }
	                   ?>
										 <!-- <tr style="background-color: transparent"> -->
											 <button class="clean green-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount(); ?>
												 <input type="hidden" name="result" value="WIN">
											 </button> 
										 <!-- </tr> -->

	                   <?php
	                 }elseif ($rate > $betCurrencyVal) {
										 // if ($timeLeft < 5) {

											$result = 'WIN';

											$tableName = array();
	 					          $tableValue =  array();
	 					          $stringType =  "";
	 					          //echo "save to database";
	 					          if($result)
	 					          {
	 					               array_push($tableName,"result_edited");
	 					               array_push($tableValue,$result);
	 					               $stringType .=  "s";
	 					          }
											if($rate)
											 {
														array_push($tableName,"end_rate");
														array_push($tableValue,$rate);
														$stringType .=  "s";
											 }
	 					          array_push($tableValue,$tradeUID);
	 					          $stringType .=  "s";
	 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
	 					          if($deductCreditAfterTrade)
	 					          {}
										 // }?>
										 <!-- <tr style="background-color: transparent"> -->

											 <button class="clean red-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
											 <input type="hidden" name="result" value="LOSE"></button>
										 <!-- </tr> -->

	                   <?php
	                 }else {
										 // if ($timeLeft < 5) {

	 									 $result = 'WIN';

	 									 $tableName = array();
	 									 $tableValue =  array();
	 									 $stringType =  "";
	 									 //echo "save to database";
	 									 if($result)
	 									 {
	 												array_push($tableName,"result_edited");
	 												array_push($tableValue,$result);
	 												$stringType .=  "s";
	 									 }
	 									 if($rate)
	 										{
	 												 array_push($tableName,"end_rate");
	 												 array_push($tableValue,$rate);
	 												 $stringType .=  "s";
	 										}
	 									 array_push($tableValue,$tradeUID);
	 									 $stringType .=  "s";
	 									 $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
	 									 if($deductCreditAfterTrade)
	 									 {}
	 									// }?>
	 									<!-- <tr style="background-color: transparent"> -->

	 										<button class="clean red-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
											<input type="hidden" name="result" value="LOSE"></button>
	 									<!-- </tr> -->
										 <?php
	                 }
	               }elseif ($betType == 'SELL') {
	                 $rate = number_format($exchangeRates['forexList'][$cnt]['bid'], 4);
	                 if ($rate > $betCurrencyVal) {
										 // if ($timeLeft < 5) {

											$result = 'LOSE';

											$tableName = array();
	 					          $tableValue =  array();
	 					          $stringType =  "";
	 					          //echo "save to database";
	 					          if($result)
	 					          {
	 					               array_push($tableName,"result_edited");
	 					               array_push($tableValue,$result);
	 					               $stringType .=  "s";
	 					          }
											if($rate)
											 {
														array_push($tableName,"end_rate");
														array_push($tableValue,$rate);
														$stringType .=  "s";
											 }
	 					          array_push($tableValue,$tradeUID);
	 					          $stringType .=  "s";
	 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
	 					          if($deductCreditAfterTrade)
	 					          {}
										 // } ?>
										 <!-- <tr style="background-color: transparent"> -->
										 	<button class="clean green-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
											<input type="hidden" name="result" value="WIN"></button>
										 <!-- </tr>   -->
										                  <?php
	                 }elseif ($rate < $betCurrencyVal) {
										 // if ($timeLeft < 5) {

											$result = 'WIN';

											$tableName = array();
	 					          $tableValue =  array();
	 					          $stringType =  "";
	 					          //echo "save to database";
	 					          if($result)
	 					          {
	 					               array_push($tableName,"result_edited");
	 					               array_push($tableValue,$result);
	 					               $stringType .=  "s";
	 					          }
											if($rate)
											 {
														array_push($tableName,"end_rate");
														array_push($tableValue,$rate);
														$stringType .=  "s";
											 }
	 					          array_push($tableValue,$tradeUID);
	 					          $stringType .=  "s";
	 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
	 					          if($deductCreditAfterTrade)
	 					          {}
										 // }
	                   ?>
										 <!-- <tr style="background-color: transparent"> -->

											 <button class="clean red-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
											 <input type="hidden" name="result" value="LOSE"></button>
										 <!-- </tr>   -->
										                 <?php
	                 }else {
										 // if ($timeLeft < 5) {

	 									 $result = 'WIN';

	 									 $tableName = array();
	 									 $tableValue =  array();
	 									 $stringType =  "";
	 									 //echo "save to database";
	 									 if($result)
	 									 {
	 												array_push($tableName,"result_edited");
	 												array_push($tableValue,$result);
	 												$stringType .=  "s";
	 									 }
	 									 if($rate)
	 										{
	 												 array_push($tableName,"end_rate");
	 												 array_push($tableValue,$rate);
	 												 $stringType .=  "s";
	 										}
	 									 array_push($tableValue,$tradeUID);
	 									 $stringType .=  "s";
	 									 $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
	 									 if($deductCreditAfterTrade)
	 									 {}
	 									// }?>
	 									<!-- <tr style="background-color: transparent"> -->

	 										<button class="clean red-down-btn border-0 three-btn-row" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betCurrency."<br>".$betStatusDetails[$i]->getAmount() ?>
											<input type="hidden" name="result" value="LOSE"></button>
	 									<!-- </tr> -->
										 <?php
	               }


	               }
	          }
	        }

	      }
	    }}
	     ?>
	    <!-- <tr style="background-color: transparent"> -->

	      <!-- <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php //echo $betStatusDetails[$i]->getTradeUid()?>">Lose</button> </td> -->
	    <!-- </tr> -->
	</div>
