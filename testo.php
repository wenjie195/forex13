<?php

// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/timezone.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/BetStatus.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://financialmodelingprep.com/api/v3/forex",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
		"x-rapidapi-host: currency-exchange.p.rapidapi.com",
		"x-rapidapi-key: c5875e8297msh333e08626d58b3fp1015afjsn73bce78f515f"
	),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
	echo "cURL Error #:" . $err;
} else {
	$exchangeRates = json_decode($response, true);
}
?>
<div  class="three-div-width">
<div class="fake-header-div">
    30 <?php echo _USERDASHBOARD_SEC ?>
  </div>
<table class="width100 data-table trade-table">
  <thead>
      <tr style="background-color: transparent">
          <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_GAIN ?></th>
            <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_LOSS ?></th>
      </tr>
    </thead>
  <tbody>
    <?php
    $betStatusDetails = getBetstatus($conn,"WHERE timeline = '30'");
    if ($betStatusDetails) {
			for ($i=0; $i <count($betStatusDetails) ; $i++) {

        $betCurrencyVal = $betStatusDetails[$i]->getStartRate();
				$tradeUID = $betStatusDetails[$i]->getTradeUid();
				$userUID = $betStatusDetails[$i]->getUid();
        $betCurrency = $betStatusDetails[$i]->getCurrency();
        $betType = $betStatusDetails[$i]->getBetType();
				$betEditBy = $betStatusDetails[$i]->getEditBy();
				$username = $betStatusDetails[$i]->getUsername();
				$betAmount = $betStatusDetails[$i]->getAmount();
				$time = date('Y-m-d H:i:s',strtotime($betStatusDetails[$i]->getDateCreated()));
				$betTimeline = $betStatusDetails[$i]->getTimeline();
				$finishTime = date('Y-m-d H:i:s',strtotime($time."+".$betTimeline."seconds"));
				$finishTimeString = strtotime($finishTime);
				$currentTime = date('Y-m-d H:i:s');
				$currentTimeString = strtotime($currentTime);
				$timeLeft = $finishTimeString - $currentTimeString;
        if (!$betEditBy && $exchangeRates && $currentTime < $finishTime) {
          for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
          {
               $allCountryCurr = $exchangeRates['forexList'][$cnt]['ticker'];
               if ($allCountryCurr == $betCurrency)
               {  if ($betType == 'BUY') {
                 $rate = number_format($exchangeRates['forexList'][$cnt]['ask'], 4);
                 if ($rate < $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'LOSE';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // }
                   ?>
									 <tr style="background-color: transparent">
										 <td><button class="clean green-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betStatusDetails[$i]->getAmount(); ?></button> </td>
									 </tr>

                   <?php
                 }elseif ($rate > $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'WIN';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // }?>
									 <tr style="background-color: transparent">
										 <td></td>
										 <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betStatusDetails[$i]->getAmount() ?></button> </td>
									 </tr>

                   <?php
                 }else {
									 // if ($timeLeft < 5) {

 									 $result = 'WIN';

 									 $tableName = array();
 									 $tableValue =  array();
 									 $stringType =  "";
 									 //echo "save to database";
 									 if($result)
 									 {
 												array_push($tableName,"result_edited");
 												array_push($tableValue,$result);
 												$stringType .=  "s";
 									 }
 									 if($rate)
 										{
 												 array_push($tableName,"end_rate");
 												 array_push($tableValue,$rate);
 												 $stringType .=  "s";
 										}
 									 array_push($tableValue,$tradeUID);
 									 $stringType .=  "s";
 									 $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 									 if($deductCreditAfterTrade)
 									 {}
 									// }?>
 									<tr style="background-color: transparent">
 										<td></td>
 										<td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betStatusDetails[$i]->getAmount() ?></button> </td>
 									</tr> <?php
                 }
               }if ($betType == 'SELL') {
                 $rate = number_format($exchangeRates['forexList'][$cnt]['bid'], 4);
                 if ($rate > $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'LOSE';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // } ?>
									 <tr style="background-color: transparent">
									 	<td><button class="clean green-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betStatusDetails[$i]->getAmount() ?></button> </td>
									 </tr>                   <?php
                 }elseif ($rate < $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'WIN';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // }
                   ?>
									 <tr style="background-color: transparent">
										 <td></td>
										 <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betStatusDetails[$i]->getAmount() ?></button> </td>
									 </tr>                  <?php
                 }else {
									 // if ($timeLeft < 5) {

 									 $result = 'WIN';

 									 $tableName = array();
 									 $tableValue =  array();
 									 $stringType =  "";
 									 //echo "save to database";
 									 if($result)
 									 {
 												array_push($tableName,"result_edited");
 												array_push($tableValue,$result);
 												$stringType .=  "s";
 									 }
 									 if($rate)
 										{
 												 array_push($tableName,"end_rate");
 												 array_push($tableValue,$rate);
 												 $stringType .=  "s";
 										}
 									 array_push($tableValue,$tradeUID);
 									 $stringType .=  "s";
 									 $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 									 if($deductCreditAfterTrade)
 									 {}
 									// }?>
 									<tr style="background-color: transparent">
 										<td></td>
 										<td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails[$i]->getTradeUid()?>"><?php echo $betStatusDetails[$i]->getUsername()."<br>".$betStatusDetails[$i]->getAmount() ?></button> </td>
 									</tr> <?php
               }


               }
          }
        }

      }
    }}
     ?>
    <tr style="background-color: transparent">

      <!-- <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php //echo $betStatusDetails[$i]->getTradeUid()?>">Lose</button> </td> -->
    </tr>
  </tbody>
</table>
</div>
<div class="three-div-width">
<div class="fake-header-div">
    60 <?php echo _USERDASHBOARD_SEC ?>
  </div>
<table class="width100 data-table trade-table">
  <thead>
      <tr style="background-color: transparent">
          <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_GAIN ?></th>
            <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_LOSS ?></th>
      </tr>
    </thead>
  <tbody>
    <?php
    $betStatusDetails60 = getBetstatus($conn,"WHERE timeline > '30' and timeline < '100'");
    if ($betStatusDetails60) {
      for ($j=0; $j <count($betStatusDetails60) ; $j++) {



        $betCurrencyVal = $betStatusDetails60[$j]->getStartRate();
        $betCurrency = $betStatusDetails60[$j]->getCurrency();
        $betType = $betStatusDetails60[$j]->getBetType();
				$betEditBy = $betStatusDetails60[$j]->getResultEdited();
				$time = date('Y-m-d H:i:s',strtotime($betStatusDetails60[$j]->getDateCreated()));
				$betTimeline = $betStatusDetails60[$j]->getTimeline();
				$finishTime = date('Y-m-d H:i:s',strtotime($time."+ 1 minutes"));
				$currentTime = date('Y-m-d H:i:s');
				$tradeUID2 = $betStatusDetails60[$j]->getTradeUid();
				$finishTimeString = strtotime($finishTime);
				$currentTimeString = strtotime($currentTime);
				$timeLeft = $finishTimeString - $currentTimeString;
        if (!$betEditBy && $exchangeRates && $currentTime < $finishTime) {
          for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
          {
               $allCountryCurr = $exchangeRates['forexList'][$cnt]['ticker'];
               if ($allCountryCurr == $betCurrency)
               {  if ($betType == 'BUY') {
                 $rate = number_format($exchangeRates['forexList'][$cnt]['ask'], 4);
                 if ($rate < $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'LOSE';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID2);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // }
                   ?>
									 <tr style="background-color: transparent">
									 	<td><button class="clean green-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails60[$j]->getTradeUid()?>"><?php echo $betStatusDetails60[$j]->getUsername()."<br>".$betStatusDetails60[$j]->getAmount() ?></button> </td>
									 </tr>                   <!--  -->
                   <?php
                 }elseif ($rate > $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'WIN';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID2);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // }
                   ?>

									 <tr style="background-color: transparent">
										 <td></td>
										 <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails60[$j]->getTradeUid()?>"><?php echo $betStatusDetails60[$j]->getUsername()."<br>".$betStatusDetails60[$j]->getAmount() ?></button> </td>
									 </tr>
                   <?php
                 }else {
									 // if ($timeLeft < 5) {

 									 $result = 'WIN';

 									 $tableName = array();
 									 $tableValue =  array();
 									 $stringType =  "";
 									 //echo "save to database";
 									 if($result)
 									 {
 												array_push($tableName,"result_edited");
 												array_push($tableValue,$result);
 												$stringType .=  "s";
 									 }
 									 if($rate)
 										{
 												 array_push($tableName,"end_rate");
 												 array_push($tableValue,$rate);
 												 $stringType .=  "s";
 										}
 									 array_push($tableValue,$tradeUID2);
 									 $stringType .=  "s";
 									 $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 									 if($deductCreditAfterTrade)
 									 {}
 									// }?>
 									<tr style="background-color: transparent">
 										<td></td>
 										<td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails60[$j]->getTradeUid()?>"><?php echo $betStatusDetails60[$j]->getUsername()."<br>".$betStatusDetails60[$j]->getAmount() ?></button> </td>
 									</tr> <?php
               }
							 if ($betType == 'SELL') {
                 $rate = number_format($exchangeRates['forexList'][$cnt]['bid'], 4);
                 if ($rate > $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'LOSE';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID2);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // }
                   ?>
									 <tr style="background-color: transparent">
									 	<td><button class="clean green-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails60[$j]->getTradeUid()?>"><?php echo $betStatusDetails60[$j]->getUsername()."<br>".$betStatusDetails60[$j]->getAmount() ?></button> </td>
									 </tr>                   <!--  -->
									 <?php
                 }elseif ($rate < $betCurrencyVal) {
									 // if ($timeLeft < 5) {

										$result = 'WIN';

										$tableName = array();
 					          $tableValue =  array();
 					          $stringType =  "";
 					          //echo "save to database";
 					          if($result)
 					          {
 					               array_push($tableName,"result_edited");
 					               array_push($tableValue,$result);
 					               $stringType .=  "s";
 					          }
										if($rate)
										 {
													array_push($tableName,"end_rate");
													array_push($tableValue,$rate);
													$stringType .=  "s";
										 }
 					          array_push($tableValue,$tradeUID2);
 					          $stringType .=  "s";
 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 					          if($deductCreditAfterTrade)
 					          {}
									 // }
                   ?>

									 <tr style="background-color: transparent">
 										<td></td>
 										<td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails60[$j]->getTradeUid()?>"><?php echo $betStatusDetails60[$j]->getUsername()."<br>".$betStatusDetails60[$j]->getAmount() ?></button> </td>
 									</tr>               <?php
                 }else {
									 // if ($timeLeft < 5) {

 									 $result = 'WIN';

 									 $tableName = array();
 									 $tableValue =  array();
 									 $stringType =  "";
 									 //echo "save to database";
 									 if($result)
 									 {
 												array_push($tableName,"result_edited");
 												array_push($tableValue,$result);
 												$stringType .=  "s";
 									 }
 									 if($rate)
 										{
 												 array_push($tableName,"end_rate");
 												 array_push($tableValue,$rate);
 												 $stringType .=  "s";
 										}
 									 array_push($tableValue,$tradeUID);
 									 $stringType .=  "s";
 									 $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
 									 if($deductCreditAfterTrade)
 									 {}
 									// }?>
 									<tr style="background-color: transparent">
 										<td></td>
 										<td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails60[$j]->getTradeUid()?>"><?php echo $betStatusDetails60[$j]->getUsername()."<br>".$betStatusDetails60[$j]->getAmount() ?></button> </td>
 									</tr> <?php
               }


               }
          }
        }

      }
    }}}
     ?>
    <tr style="background-color: transparent">

      <!-- <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php //echo $betStatusDetails60[$j]->getTradeUid()?>">Lose</button> </td> -->
    </tr>
  </tbody>
</table>
	</div>

	<div class="three-div-width">
	<div class="fake-header-div">
	    180 <?php echo _USERDASHBOARD_SEC ?>
	  </div>
	<table class="width100 data-table trade-table">
	  <thead>
	      <tr style="background-color: transparent">
	          <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_GAIN ?></th>
	            <th><?php echo _ADMINCURRENTTRADE_UNREALIZED_LOSS ?></th>
	      </tr>
	    </thead>
	  <tbody>
	    <?php
	    $betStatusDetails180 = getBetstatus($conn,"WHERE timeline = '180'");
	    if ($betStatusDetails180) {
	      for ($k=0; $k <count($betStatusDetails180) ; $k++) {



	        $betCurrencyVal = $betStatusDetails180[$k]->getStartRate();
	        $betCurrency = $betStatusDetails180[$k]->getCurrency();
	        $betType = $betStatusDetails180[$k]->getBetType();
					$betEditBy = $betStatusDetails180[$k]->getResultEdited();
					$time = date('Y-m-d H:i:s',strtotime($betStatusDetails180[$k]->getDateCreated()));
					$betTimeline = $betStatusDetails180[$k]->getTimeline();
					$finishTime = date('Y-m-d H:i:s',strtotime($time."+".$betTimeline."seconds"));
					$currentTime = date('Y-m-d H:i:s');
					$tradeUID3 = $betStatusDetails180[$k]->getTradeUid();
					$finishTimeString = strtotime($finishTime);
					$currentTimeString = strtotime($currentTime);
					$timeLeft = $finishTimeString - $currentTimeString;
	        if (!$betEditBy && $exchangeRates && $currentTime < $finishTime) {
	          for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
	          {
	               $allCountryCurr = $exchangeRates['forexList'][$cnt]['ticker'];
	               if ($allCountryCurr == $betCurrency)
	               {  if ($betType == 'BUY') {
	                 $rate = number_format($exchangeRates['forexList'][$cnt]['ask'], 4);
	                 if ($rate < $betCurrencyVal) {
										 // if ($timeLeft < 5) {

											$result = 'LOSE';

											$tableName = array();
	 					          $tableValue =  array();
	 					          $stringType =  "";
	 					          //echo "save to database";
	 					          if($result)
	 					          {
	 					               array_push($tableName,"result_edited");
	 					               array_push($tableValue,$result);
	 					               $stringType .=  "s";
	 					          }
											if($rate)
											 {
														array_push($tableName,"end_rate");
														array_push($tableValue,$rate);
														$stringType .=  "s";
											 }
	 					          array_push($tableValue,$tradeUID3);
	 					          $stringType .=  "s";
	 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
	 					          if($deductCreditAfterTrade)
	 					          {}
										 // }
	                   ?>
										 <tr style="background-color: transparent">
										 	<td><button class="clean green-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails180[$k]->getTradeUid()?>"><?php echo $betStatusDetails180[$k]->getUsername()."<br>".$betStatusDetails180[$k]->getAmount() ?></button> </td>
										 </tr>                   <!--  -->
	                   <?php
	                 }elseif ($rate > $betCurrencyVal) {
										 // if ($timeLeft < 5) {

											$result = 'WIN';

											$tableName = array();
	 					          $tableValue =  array();
	 					          $stringType =  "";
	 					          //echo "save to database";
	 					          if($result)
	 					          {
	 					               array_push($tableName,"result_edited");
	 					               array_push($tableValue,$result);
	 					               $stringType .=  "s";
	 					          }
											if($rate)
											 {
														array_push($tableName,"end_rate");
														array_push($tableValue,$rate);
														$stringType .=  "s";
											 }
	 					          array_push($tableValue,$tradeUID3);
	 					          $stringType .=  "s";
	 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
	 					          if($deductCreditAfterTrade)
	 					          {}
										 // }
	                   ?>
										 <tr style="background-color: transparent">
											 <td></td>
											 <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails180[$k]->getTradeUid()?>"><?php echo $betStatusDetails180[$k]->getUsername()."<br>".$betStatusDetails180[$k]->getAmount() ?></button> </td>
										 </tr>
	                   <?php
	                 }else {
										 // if ($timeLeft < 5) {

	 									 $result = 'WIN';

	 									 $tableName = array();
	 									 $tableValue =  array();
	 									 $stringType =  "";
	 									 //echo "save to database";
	 									 if($result)
	 									 {
	 												array_push($tableName,"result_edited");
	 												array_push($tableValue,$result);
	 												$stringType .=  "s";
	 									 }
	 									 if($rate)
	 										{
	 												 array_push($tableName,"end_rate");
	 												 array_push($tableValue,$rate);
	 												 $stringType .=  "s";
	 										}
	 									 array_push($tableValue,$tradeUID);
	 									 $stringType .=  "s";
	 									 $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
	 									 if($deductCreditAfterTrade)
	 									 {}
	 									// }?>
	 									<tr style="background-color: transparent">
	 										<td></td>
	 										<td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails180[$k]->getTradeUid()?>"><?php echo $betStatusDetails180[$k]->getUsername()."<br>".$betStatusDetails180[$k]->getAmount() ?></button> </td>
	 									</tr> <?php
	               }
								 if ($betType == 'SELL') {
	                 $rate = number_format($exchangeRates['forexList'][$cnt]['bid'], 4);
	                 if ($rate > $betCurrencyVal) {
										 // if ($timeLeft < 5) {

											$result = 'LOSE';

											$tableName = array();
	 					          $tableValue =  array();
	 					          $stringType =  "";
	 					          //echo "save to database";
	 					          if($result)
	 					          {
	 					               array_push($tableName,"result_edited");
	 					               array_push($tableValue,$result);
	 					               $stringType .=  "s";
	 					          }
											if($rate)
											 {
														array_push($tableName,"end_rate");
														array_push($tableValue,$rate);
														$stringType .=  "s";
											 }
	 					          array_push($tableValue,$tradeUID3);
	 					          $stringType .=  "s";
	 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
	 					          if($deductCreditAfterTrade)
	 					          {}
										 // }
	                   ?>
										 <tr style="background-color: transparent">
										 	<td><button class="clean green-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails180[$k]->getTradeUid()?>"><?php echo $betStatusDetails180[$k]->getUsername()."<br>".$betStatusDetails180[$k]->getAmount() ?></button> </td>
										 </tr>                   <!--  -->
										 <?php
	                 }elseif ($rate < $betCurrencyVal) {
										 // if ($timeLeft < 5) {

											$result = 'WIN';

											$tableName = array();
	 					          $tableValue =  array();
	 					          $stringType =  "";
	 					          //echo "save to database";
	 					          if($result)
	 					          {
	 					               array_push($tableName,"result_edited");
	 					               array_push($tableValue,$result);
	 					               $stringType .=  "s";
	 					          }
											if($rate)
											 {
														array_push($tableName,"end_rate");
														array_push($tableValue,$rate);
														$stringType .=  "s";
											 }
	 					          array_push($tableValue,$tradeUID3);
	 					          $stringType .=  "s";
	 					          $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
	 					          if($deductCreditAfterTrade)
	 					          {}
										 // }
	                   ?>
										 <tr style="background-color: transparent">
											 <td></td>
											 <td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails180[$k]->getTradeUid()?>"><?php echo $betStatusDetails180[$k]->getUsername()."<br>".$betStatusDetails180[$k]->getAmount() ?></button> </td>
										 </tr>                  <?php
	                 }else {
										 // if ($timeLeft < 5) {

	 									 $result = 'WIN';

	 									 $tableName = array();
	 									 $tableValue =  array();
	 									 $stringType =  "";
	 									 //echo "save to database";
	 									 if($result)
	 									 {
	 												array_push($tableName,"result_edited");
	 												array_push($tableValue,$result);
	 												$stringType .=  "s";
	 									 }
	 									 if($rate)
	 										{
	 												 array_push($tableName,"end_rate");
	 												 array_push($tableValue,$rate);
	 												 $stringType .=  "s";
	 										}
	 									 array_push($tableValue,$tradeUID);
	 									 $stringType .=  "s";
	 									 $deductCreditAfterTrade = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
	 									 if($deductCreditAfterTrade)
	 									 {}
	 									// }?>
	 									<tr style="background-color: transparent">
	 										<td></td>
	 										<td><button class="clean red-down-btn border-0 width100-ow" type="submit" name="trading_uid" value="<?php echo $betStatusDetails180[$k]->getTradeUid()?>"><?php echo $betStatusDetails180[$k]->getUsername()."<br>".$betStatusDetails180[$k]->getAmount() ?></button> </td>
	 									</tr> <?php
	               }


	               }


	          }
	        }

	      }
	    }}}

	     ?>

	  </tbody>
	</table>
	</div>
