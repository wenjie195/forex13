<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Withdrawal.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/withdrawHistory.php" />
    <meta property="og:title" content="Withdraw History | De Xin Guo Ji 德鑫国际" />
    <title>Withdraw History | De Xin Guo Ji 德鑫国际</title>
    <meta property="og:url" content="https://dxforextrade88.com/withdrawHistory.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAfterLogin.php'; ?>
    <div class="overflow small-web-width menu-distance">
    <h1 class="white-text history-title"><?php echo _PROFILE_WITHDRAW_HISTORY ?></h1>
		<table class="table-width data-table">
        	<thead>
            	<tr>
                	<th><?php echo _AUD_AMOUNT ?></th>
                    <th><?php echo _TOPUP_HISTORY_DATE ?></th>
                    <th><?php echo _WITHDRAWAL_VERIFY ?></th>
                </tr>
            </thead> 
            <!-- <tbody>
            	<tr>
                	<td>1000</td>
                    <td>1/1/2020 10:00</td>
                </tr>
            	<tr>
                	<td>1000</td>
                    <td>1/1/2020 10:00</td>
                </tr>                
            </tbody> -->

            <tbody>
                <?php
                $conn = connDB();
                $withdrawalDetails = getWithdrawal($conn,"WHERE uid = ?", array("uid") ,array($uid),"s");
                if($withdrawalDetails != null)
                {   
                    for($cnt = 0;$cnt < count($withdrawalDetails) ;$cnt++)
                    {?>
                    <tr>
                        <!-- <td><?php //echo ($cnt+1)?></td>
                        <td><?php //echo $withdrawalDetails[$cnt]->getUsername();?></td> -->
                        <td><?php echo $withdrawalDetails[$cnt]->getAmount();?></td>
                        <td>
                            <!-- <?php //$dateCreated = date("Y-m-d H:i",strtotime($withdrawalDetails[$cnt]->getDateCreated())); echo $dateCreated;?> -->
                            <?php $dateCreated = date("Y-m-d",strtotime($withdrawalDetails[$cnt]->getDateCreated())); echo $dateCreated;?>
                        </td>
                        <!-- <td><?php //echo $withdrawalDetails[$cnt]->getStatus();?></td> -->
                        <td>
                            <?php 
                                $withdrawalCurrentStatus = $withdrawalDetails[$cnt]->getStatus();
                                if($withdrawalCurrentStatus == 'PENDING')
                                {
                                    echo "待处理";
                                }
                                elseif($withdrawalCurrentStatus == 'REJECTED')
                                {
                                    echo "提现失败";
                                }
                                elseif($withdrawalCurrentStatus == 'ACCEPTED')
                                {
                                    echo "已批准";
                                }
                            ?>
                        </td>
                    <?php
                    }?>
                    </tr>
                <?php
                }

                ?>
            </tbody>

         </table>   
    </div> 
</div>

<div class="clear"></div>
<?php include 'js.php'; ?>

</body>

</html>
