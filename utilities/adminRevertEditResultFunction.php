<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/BetStatus.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function submitEditRecord($conn,$tradeUid,$tradeUserUid,$tradeAmount,$tradeResult,$resultEdited,$adminUsername)
{
     if(insertDynamicData($conn,"edit_record",array("trade_uid","uid","amount","result","result_edited","edit_by"),
     array($tradeUid,$tradeUserUid,$tradeAmount,$tradeResult,$resultEdited,$adminUsername),"ssisss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = $_SESSION['uid'];
    $adminDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
    $adminUsername = $adminDetails[0]->getUsername();

    $tradeUid = rewrite($_POST["trading_uid"]);
    $tradeUserUid = rewrite($_POST["trading_userid"]);
    $tradeId = rewrite($_POST["trading_id"]);
    $tradeAmount = rewrite($_POST["trading_amount"]);
    $tradeResult = rewrite($_POST["trading_result"]);
    $tradeEditResult = rewrite($_POST["trading_editresult"]);

    if($tradeEditResult == 'LOSE')
    {
        $resultEdited = 'WIN';
        $statusEditedAgain = 'EDITED AGAIN';
        //get user's uid and other details
        $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($tradeUserUid),"s");
        $userCredit = $userDetails[0]->getCredit();
        //no win lose
        // $userUpdateCredit = $userCredit + $tradeAmount;
        //become win
        $userUpdateCredit = $userCredit + $tradeAmount + $tradeAmount;
    }
    elseif($tradeEditResult == 'WIN')
    {
        $resultEdited = 'LOSE';
        $statusEditedAgain = 'EDITED AGAIN';
        //get user's uid and other details
        $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($tradeUserUid),"s");
        $userCredit = $userDetails[0]->getCredit();
        //no win lose
        // $userUpdateCredit = $userCredit - $tradeAmount;
        //become lose
        $userUpdateCredit = $userCredit - $tradeAmount - $tradeAmount;
    }

    // for debugging
    // echo "<br>";
    // echo $adminUsername."<br>";
    // echo $tradeUid."<br>";
    // echo $tradeUserUid."<br>";
    // echo $tradeId."<br>";
    // echo $tradeAmount."<br>";
    // echo $tradeResult."<br>";
    // echo $tradeEditResult."<br>";
    // echo $resultEdited."<br>";
    // echo $statusEditedAgain."<br>";
    // echo $userCredit."<br>";
    // echo $userUpdateCredit."<br>";

    if(isset($_POST['trading_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($resultEdited)
        {
            array_push($tableName,"result_edited");
            array_push($tableValue,$resultEdited);
            $stringType .=  "s";
        }   
        if($statusEditedAgain)
        {
            array_push($tableName,"status");
            array_push($tableValue,$statusEditedAgain);
            $stringType .=  "s";
        }  
        if($adminUsername)
        {
            array_push($tableName,"edit_by");
            array_push($tableValue,$adminUsername);
            $stringType .=  "s";
        }   

        array_push($tableValue,$tradeUid);
        $stringType .=  "s";
        $resultUpdated = updateDynamicData($conn,"bet_status"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
        
        if($resultUpdated)
        {
            echo "success";
            echo "<br>";

            if(isset($_POST['trading_uid']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($userUpdateCredit)
                {
                    array_push($tableName,"credit");
                    array_push($tableValue,$userUpdateCredit);
                    $stringType .=  "s";
                }    
          
                array_push($tableValue,$tradeUserUid);
                $stringType .=  "s";
                $orderUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                
                if($orderUpdated)
                {
                    // echo "success aa";

                    if(submitEditRecord($conn,$tradeUid,$tradeUserUid,$tradeAmount,$tradeResult,$resultEdited,$adminUsername))
                    {
                        header('Location: ../adminEditedCurrentTrade.php?EditedandSave');
                    }
                    else
                    {
                        header('Location: ../adminEditedCurrentTrade.php?EditedbutNoSave');
                    }

                }
                else
                {
                    // echo "fail aa";
                    // echo "<br>";
                    header('Location: ../adminEditedCurrentTrade.php?FailToEdited');
                }
            }
            else
            {
                echo "dunno aa";
                echo "<br>";
            }
        }
        else
        {
            echo "fail";
            echo "<br>";
        }
    }
    else
    {
        echo "dunno";
        echo "<br>";
    }
}
else 
{
    header('Location: ../index.php');
}

?>