<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Message.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

$senderUID = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = $senderUID;
     $message_uid = md5(uniqid());
     $receiveSMS = $_FILES['file']['name'];
     $adminStatus = "GET";
     $userStatus = "SENT";
     $updateMessageStatus = "YES";
     $replyOne = $message_uid;
     $replyTwo = "YES";
     $replyThree = "NEW";

     // $target_dir = "../uploads/";
     $target_dir = "../uploads/";
     $target_file = $target_dir . basename($_FILES["file"]["name"]);
     
     // Select file type
     $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
     // Valid file extensions
     $extensions_arr = array("jpg","jpeg","png","gif","pdf");

     if( in_array($imageFileType,$extensions_arr) )
     {
          move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$receiveSMS);
     }

     // //for debugging
     // echo "<br>";
     // echo $uid."<br>";
     // echo $message_uid."<br>";

     if(isset($_POST['submit']))
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database"; 
          if($updateMessageStatus)
          {
               array_push($tableName,"message");
               array_push($tableValue,$updateMessageStatus);
               $stringType .=  "s";
          } 
          array_push($tableValue,$uid);
          $stringType .=  "s";
          $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($messageStatusInUser)
          {
               // header('Location: ../viewMessage.php?type=1');
               if(submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne,$replyTwo,$replyThree))
               {
                    header('Location: ../viewMessage.php?type=1');
               }
               else
               {
                    header('Location: ../viewMessage.php?type=2');
               }
          }
          else
          {
               header('Location: ../viewMessage.php?type=3');
          }
     }
     else
     {
     header('Location: ../viewMessage.php?type=4');
     }

     // if(submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne,$replyTwo))
     // {
     //      // header('Location: ../viewMessage.php?type=1');

     //      if(isset($_POST['submit']))
     //      {   
     //           $tableName = array();
     //           $tableValue =  array();
     //           $stringType =  "";
     //           //echo "save to database"; 
     //           if($updateMessageStatus)
     //           {
     //                array_push($tableName,"message");
     //                array_push($tableValue,$updateMessageStatus);
     //                $stringType .=  "s";
     //           } 
     //           array_push($tableValue,$uid);
     //           $stringType .=  "s";
     //           $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
     //           if($messageStatusInUser)
     //           {
     //                header('Location: ../viewMessage.php?type=1');
     //           }
     //           else
     //           {
     //                header('Location: ../viewMessage.php?type=2');
     //           }
     //      }
     //      else
     //      {
     //      header('Location: ../viewMessage.php?type=3');
     //      }

     // }
     // else
     // {
     //      header('Location: ../viewMessage.php?type=4');
     // }

}
else
{
     header('Location: ../index.php');
}

function submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne,$replyTwo,$replyThree)
{
     if(insertDynamicData($conn,"message",array("uid","message_uid","receive_message","user_status","admin_status","reply_one","reply_two","reply_three"),
     array($uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne,$replyTwo,$replyThree),"ssssssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

?>