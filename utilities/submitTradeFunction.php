<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../timezone.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

//trade with end rate
function submitTrade($conn,$tradeUID,$userUID,$rate,$endRate,$currency,$amount,$type,$timeline,$result,$currentCredit,$userUsername,$dateCreated)
{
     if(insertDynamicData($conn,"record",array("trade_uid","uid","start_rate","end_rate","currency","amount","bet_type","timeline","result","current_credit","username","date_created"),
     array($tradeUID,$userUID,$rate,$endRate,$currency,$amount,$type,$timeline,$result,$currentCredit,$userUsername,$dateCreated),"sssssisissss") === null)

     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}
function submitBetStatus($conn,$tradeUID,$userUID,$rate,$endRate,$currency,$amount,$type,$timeline,$result,$currentCredit,$userUsername,$resultEdited,$dateCreated)
{

     if(insertDynamicData($conn,"bet_status",array("trade_uid","uid","start_rate","end_rate","currency","amount","bet_type","timeline","result","current_credit","username","result_edited","date_created"),
     array($tradeUID,$userUID,$rate,$endRate,$currency,$amount,$type,$timeline,$result,$currentCredit,$userUsername,$resultEdited,$dateCreated),"sssssisisssss") === null)

     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     //get ticker rating
     $curl = curl_init();
     curl_setopt_array($curl, array(
          CURLOPT_URL => "https://financialmodelingprep.com/api/v3/forex",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
               "x-rapidapi-host: currency-exchange.p.rapidapi.com",
               "x-rapidapi-key: c5875e8297msh333e08626d58b3fp1015afjsn73bce78f515f"
          ),
     ));

     $response = curl_exec($curl);
     $err = curl_error($curl);
     curl_close($curl);

     if ($err)
     {
          echo "cURL Error #:" . $err;
     }
     else
     {
          $exchangeRates = json_decode($response, true);
     }


     $tradeUID = md5(uniqid());
     $type = rewrite($_POST["trade_type"]);
     $dateCreated = date('Y-m-d H:i:s');
     $timeline = rewrite($_POST["trade_timeline"]);
     $timeDate = ($_POST["date"]);
     $userUID = rewrite($_POST["user_uid"]);
     $amount1 = rewrite($_POST["trade_amount"]);

     $playTime = rewrite($_POST["timer"]);
     $amount2 = rewrite($_POST["trade_other_amount"]);
     $currency = rewrite($_POST["trade_currency"]);

     if ($exchangeRates)
     {
          for ($cnt=0; $cnt <count($exchangeRates['forexList']) ; $cnt++)
          {
               $allCountryCurr = $exchangeRates['forexList'][$cnt]['ticker'];
               if ($allCountryCurr == $currency)
               {
                    $rate = number_format($exchangeRates['forexList'][$cnt]['ask'], 4);
               }
          }
     }
     $endRate = "";
     $result = "";


     $userDetails = getUser($conn," WHERE uid = ? ",array("uid"),array($userUID),"s");
     $userCurrentCredit = $userDetails[0]->getCredit();
     $userUsername = $userDetails[0]->getUsername();

     if ($amount1 && $userCurrentCredit >= $amount1 ) {
       $amountFinal = $userCurrentCredit - $amount1;
         $amount = $amount1;
     }
     elseif($amount2 && $userCurrentCredit >= $amount2 ) {
       $amountFinal = $userCurrentCredit - $amount2;
       $amount = $amount2;
     }else {
       $amount = null;
     }
     // echo $amount;
if ($amount) {

     if(isset($_POST['submit_trade']))
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($amountFinal || !$amountFinal)
          {
               array_push($tableName,"credit");
               array_push($tableValue,$amountFinal);
               $stringType .=  "s";
          }
          array_push($tableValue,$userUID);
          $stringType .=  "s";
          $deductCreditAfterTrade = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($deductCreditAfterTrade)
          {
            if(submitTrade($conn,$tradeUID,$userUID,$rate,$endRate,$currency,$amount,$type,$timeline,$result,$amountFinal,$userUsername,$dateCreated))
            {
            // echo "1st data insert<br>";
                 if(submitBetStatus($conn,$tradeUID,$userUID,$rate,$endRate,$currency,$amount,$type,$timeline,$result,$amountFinal,$userUsername,$result,$dateCreated))
                 {

                   // header('Location: ../userDashboard.php?type=1');
                      $_SESSION['messageType'] = 1;
                      header('Location: ../userDashboard.php?type=4');
                      // echo "<script>alert('successfully !');window.location='../userDashboard.php'</script>";
                    }
          }
        }
      }else {
        $_SESSION['messageType'] = 1;
        header('Location: ../userDashboard.php?type=7');
      }
}else {
  $_SESSION['messageType'] = 1;
  header('Location: ../userDashboard.php?type=7');
}






}else {
  header('location: ../userDashboard.php');
}
//

?>
