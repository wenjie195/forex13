<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Message.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne)
{
     if(insertDynamicData($conn,"message",array("uid","message_uid","receive_message","user_status","admin_status","reply_one"),
     array($uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne),"ssssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $message_uid = md5(uniqid());
     $uid = rewrite($_POST["sender_uid"]);
     $receiveSMS = rewrite($_POST["message_details"]);
     $userStatus = "SENT";
     $adminStatus = "GET";
     $updateMessageStatus = "YES";
     $replyOne = $message_uid;

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $message_uid."<br>";
     // echo $uid."<br>";
     // echo $receiveSMS."<br>";

     if(isset($_POST['sent_message']))
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database"; 
          if($updateMessageStatus)
          {
               array_push($tableName,"message");
               array_push($tableValue,$updateMessageStatus);
               $stringType .=  "s";
          } 
          array_push($tableValue,$uid);
          $stringType .=  "s";
          $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($messageStatusInUser)
          {
               if(submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne))
               {
                    header('Location: ../viewMessage.php?type=1');
               }
               else
               {
                    header('Location: ../viewMessage.php?type=2');
               }
          }
          else
          {
          header('Location: ../viewMessage.php?type=3');
          }
     }
     else
     {
     header('Location: ../viewMessage.php?type=4');
     }


     // if(submitMessage($conn,$uid,$message_uid,$receiveSMS,$userStatus,$adminStatus,$replyOne))
     // {
     //      header('Location: ../viewMessage.php?type=1');
     // }
     // else
     // {
     //      header('Location: ../viewMessage.php?type=2');
     // }

}
else
{
     header('Location: ../viewMessage.php?type=3');
}
?>
