<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/Message.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

$senderUID = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

     $uid = $senderUID;
     $updateMessageStatus = "NO";
     $previousMessageStatus = "OLD";

     // $adminStatus = "GET";
     // $userStatus = "SENT";

     //     //for debugging
     //     echo "<br>";
     //     echo $uid."<br>";
     //     echo $message_uid."<br>";
     //     echo $receiveSMS."<br>";
     //     echo $adminStatus."<br>";
     //     echo $userStatus."<br>";

     if(isset($_POST['read_sms']))
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database"; 
          if($updateMessageStatus)
          {
               array_push($tableName,"message");
               array_push($tableValue,$updateMessageStatus);
               $stringType .=  "s";
          } 
          array_push($tableValue,$uid);
          $stringType .=  "s";
          $messageStatusInUser = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($messageStatusInUser)
          {
               // header('Location: ../viewMessage.php?type=1');

               if(isset($_POST['read_sms']))
               {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database"; 
                    if($previousMessageStatus)
                    {
                         array_push($tableName,"reply_three");
                         array_push($tableValue,$previousMessageStatus);
                         $stringType .=  "s";
                    } 
                    array_push($tableValue,$uid);
                    $stringType .=  "s";
                    $messageStatusInUser = updateDynamicData($conn,"message"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($messageStatusInUser)
                    {
                         // header('Location: ../profile.php');
                         header('Location: ../profile.php?type=11');
                    }
                    else
                    {
                         header('Location: ../viewMessage.php?type=2');
                    }
               }
               else
               {
               header('Location: ../viewMessage.php?type=3');
               }

          }
          else
          {
               header('Location: ../viewMessage.php?type=4');
          }
     }
     else
     {
     header('Location: ../viewMessage.php?type=5');
     }
}
else 
{
    // echo "gg";
    header('Location: ../index.php');
}

?>