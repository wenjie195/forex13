<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function submitWithdrawal($conn,$uid,$name,$bankName,$bankAccNumber,$amount,$currentCredit,$status,$nationality)
{
     if(insertDynamicData($conn,"withdrawal",array("uid","username","bank_name","bank_acc_number","amount","current_credit","status","contact"),
     array($uid,$name,$bankName,$bankAccNumber,$amount,$currentCredit,$status,$nationality),"ssssssss") === null)
     {
          // echo "aaaa";
     }
     else
     {
          // echo "bbbb";
     }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = rewrite($_POST["withdrawal_uid"]);
     $name = rewrite($_POST["withdrawal_bank_acc_holder"]);
     $bankName = rewrite($_POST["withdrawal_bank_name"]);
     $bankAccNumber = rewrite($_POST["withdrawal_bank_acc_number"]);
     $amount = rewrite($_POST["withdrawal_amount"]);
     $currentCredit = rewrite($_POST["withdrawal_currentcredit"]);

     //     $nationality = rewrite($_POST['nationality']);

     $nationality = rewrite($_POST['trade_type']);

     $status = "PENDING";
     // $status = "待处理";

     $latestCredit = $currentCredit - $amount;

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $uid."<br>";
     // echo $name."<br>";
     // echo $bankName."<br>";
     // echo $amount."<br>";
     // echo $currentCredit."<br>";
     // echo $status."<br>";
     // echo $latestCredit."<br>";

     if($amount < 50000)
     {
          // echo "amount less than 50k" ;
          // echo "提现数额至少5万！" ;
          echo "<script>alert('提现数额至少5万！');window.location='../profile.php'</script>";
     }
     else
     {

          if($amount > $currentCredit)
          {
               // echo "withdrawal amount more than wallet amount" ;
               // echo "提现数额已超越余额！" ;
               echo "<script>alert('提现数额已超越余额！');window.location='../profile.php'</script>";
          }
          else
          {
               // echo "submit withdrawal" ; 

               if(submitWithdrawal($conn,$uid,$name,$bankName,$bankAccNumber,$amount,$currentCredit,$status,$nationality))
               {
                    // $_SESSION['messageType'] = 1;
                    // header('Location: ../admin1Product.php?type=4');
                    // echo "submit withdrawal success";
          
                    if(isset($_POST['withdrawal_uid']))
                    {
                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         //echo "save to database";
                         if($latestCredit)
                         {
                              array_push($tableName,"credit");
                              array_push($tableValue,$latestCredit);
                              $stringType .=  "s";
                         }
                         if(!$latestCredit)
                         {    
                              $latestCredit = 0;
                              array_push($tableName,"credit");
                              array_push($tableValue,$latestCredit);
                              $stringType .=  "s";
                         }
                         array_push($tableValue,$uid);
                         $stringType .=  "s";
                         $orderUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);

                         if($orderUpdated)
                         {
                              // $_SESSION['messageType'] = 1;
                              // header('Location: ../adminShipping.php?type=11');
                              // echo "<script>alert('Submit Withdrawal Request Successfully !');window.location='../userDashboard.php'</script>";
                              // echo "<script>alert('成功提交提现手续');window.location='../profile.php'</script>";
                              echo "<script>alert('谢谢！');window.location='../profile.php'</script>";
                         }
                         else
                         {
                              //echo "fail aa";
                              // echo "<script>alert('Unable to deduct credit in user side !');window.location='../userDashboard.php'</script>";
                              echo "<script>alert('无法扣除余额！');window.location='../profile.php'</script>";
                         }
                    }
                    else
                    {
                         echo "<script>alert('系统出现了问题 !');window.location='../profile.php'</script>";
                    }
          
          
               }
               else
               {
                    // echo "<script>alert('Fail to Submit Withdrawal Request !');window.location='../userDashboard.php'</script>";
                    echo "<script>alert('无法提交提现手续！');window.location='../profile.php'</script>";
               }

          }

     }

}
else
{
     // echo "<script>alert('Server Problem !');window.location='../index.php'</script>";
     header('Location: ../index.php');
}
?>