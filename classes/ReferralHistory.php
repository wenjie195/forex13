<?php
class ReferralHistory {
    /* Member variables */
    var $id,$referrerId,$referralId,$referralName,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getReferrerId()
    {
        return $this->referrerId;
    }

    /**
     * @param mixed $referrerId
     */
    public function setReferrerId($referrerId)
    {
        $this->referrerId = $referrerId;
    }

    /**
     * @return mixed
     */
    public function getReferralName()
    {
        return $this->referralName;
    }

    /**
     * @param mixed $referralName
     */
    public function setReferralName($referralName)
    {
        $this->referralName = $referralName;
    }

    /**
     * @return mixed
     */
    public function getReferralId()
    {
        return $this->referralId;
    }

    /**
     * @param mixed $referralId
     */
    public function setReferralId($referralId)
    {
        $this->referralId = $referralId;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getReferralHistory($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","referrer_id","referral_id","referral_name","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"referral_history");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('si',$queryValues[0],$queryValues[1]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id, $referrerId, $referralId, $referralName, $dateCreated, $dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $referralHistory = new ReferralHistory();
            $referralHistory->setId($id);
            $referralHistory->setReferrerId($referrerId);
            $referralHistory->setReferralId($referralId);
            $referralHistory->setreferralName($referralName);
            $referralHistory->setDateCreated($dateCreated);
            $referralHistory->setDateUpdated($dateUpdated);

            array_push($resultRows,$referralHistory);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}