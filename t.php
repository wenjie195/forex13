<?php echo '<script type="text/javascript" src="js/vue.min.js"></script>'; ?>
<?php echo '<script type="text/javascript" src="js/axios.min.js"></script>'; ?>
<?php echo '<script type="text/javascript" src="js/echarts.min.js"></script>'; ?>
<style media="screen">
  #positionBtn{
    margin-left: 420px;
    padding-top: 10px;
  }
  @media all and (min-height: 650px){
  #positionBtn{
      margin-left: 440px;
  }
  }
</style>
<div id="app">
  <div id="positionBtn" role="group" aria-label="Basic example" class="btn-group btn-group-zoom">
    <!---->
    <button type="button" class="btn" @click="setPeriod(5)">
    5D</button>
    <button type="button" class="btn" @click="setPeriod(30)">
    1M</button>
    <button type="button" class="btn" @click="setPeriod(180)">
    6M</button>
    <button type="button" class="btn" @click="setPeriod(365)">
    1Y</button>
    <button type="button" class="btn" @click="setPeriod(1825)">
    5Y</button>
    <button type="button" class="btn">MAX</button>
    <!---->
    <button type="button" class="btn" @click="createLineChart()">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 595.3 841.9" enable-background="new 0 0 595.3 841.9" width="26" height="26"><path d="M142.5 447.4c-102.74 102.74-83.44 83.44 0 0m399-186.2l-70.9 94.6H370.1l-136 159.6-91.6-112.3-38.5 41.4L6.5 542l41.4 41.4 94.6-94.6 91.6 112.3 162.6-186.2h103.5l88.7-118.2z" opacity="1"></path><!----></svg>
    </button>
    <!---->
    <button type="button" class="btn" @click="createCandlestickChart()">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 26" enable-background="new 0 0 595.3 841.9" width="26" height="26"><path d="M16 3v3h-2v12h2v5h1v-5h2V6h-2V3h-1zM9 4v5H7v11h2v3h1v-3h2V9h-2V4H9z" opacity="1"></path><!----></svg>
    </button>
    <!---->
    <button type="button" class="btn" @click="createLineChart('area')">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 595.3 841.9" enable-background="new 0 0 595.3 841.9" width="26" height="26"><path d="M453.8 360.2l-92.1 6L234 517.7l-86.2-86.1L5.2 514.7v118.9h585.3V268.1z" opacity="0.3"></path><path d="M234 508.8c-156 222.067-78 111.033 0 0zm309-276.3l-71.3 95.1h-101L236.9 488l-92.1-112.9-38.6 41.6-101 98.1 41.6 41.6 95.1-95.1L234 574.2 397.4 387h104l89.1-118.9z"></path></svg>
    </button>
    <!---->
    <button type="button" class="btn btn-padding">
    <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 24 24"><path d="M21.414 18.586l2.586-2.586v8h-8l2.586-2.586-5.172-5.172 2.828-2.828 5.172 5.172zm-13.656-8l2.828-2.828-5.172-5.172 2.586-2.586h-8v8l2.586-2.586 5.172 5.172zm10.828-8l-2.586-2.586h8v8l-2.586-2.586-5.172 5.172-2.828-2.828 5.172-5.172zm-8 13.656l-2.828-2.828-5.172 5.172-2.586-2.586v8h8l-2.586-2.586 5.172-5.172z"></path></svg>
    </button>
    <button @click=" hasOption = !hasOption;createCandlestickChart()">Indicators</button>
  </div>
  <!---->
  <div id="main" style="width: 1350px;height:550px;"></div>
</div>
<script type="text/javascript">
// https://financialmodelingprep.com/developer/docs
var app = new Vue({
el: "#app",
data: {
  url: 'https://financialmodelingprep.com/api/company/historical-price/AAPL?serietype=candle&datatype=json&serieformat=array',
  urlLine: 'https://financialmodelingprep.com/api/company/historical-price/AAPL?serietype=line&datatype=json&serieformat=array',
  financials: [],
  financialsLine: [],
  upColor: '#ec0000',
  upBorderColor: '#8A0000',
  downColor: '#00da3c',
  downBorderColor: '#008F28',
  selectedChart: 'candlestick',
  period: 98,
  hasOption: false,
},
created() {
  var self = this
  axios
    .get(this.url)
    .then((response) => {

      this.financials = response.data.historical;
      this.createCandlestickChart();

    })
    .catch((error) => {
      alert('error');
    })

  axios
    .get(this.urlLine)
    .then((response) => {

      this.financialsLine = response.data.historical;

    })
    .catch((error) => {
      alert('error');
    })
},

methods: {

  createCandlestickChart() {

    this.selectedChart = 'candlestick';

    let data = this.splitData([...this.financials]);

    let option = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross'
        }
      },
      legend: {
        data: ['MA5', 'MA10', 'MA20', 'MA30']
      },
      grid: {
        left: '10%',
        right: '10%',
        bottom: '15%'
      },
      xAxis: {
        type: 'category',
        data: data.categoryData,
        scale: true,
        boundaryGap: true,
        axisLine: {
          onZero: false
        },
        splitLine: {
          show: false
        },
        splitNumber: 20,
        min: 'dataMin',
        max: 'dataMax'
      },
      yAxis: {
        scale: true,
        splitArea: {
          show: true
        },
        axisLabel: {
          formatter: (param) => {
            return '$ ' + param;
          }
        }
      },
      dataZoom: [{
          type: 'inside',
          start: this.period,
          end: 100
        },
        {
          show: true,
          type: 'slider',
          y: '90%',
          start: 50,
          end: 100
        }
      ],
      series: [{
          type: 'candlestick',
          data: data.values,
          itemStyle: {
            normal: {
              color: this.upColor,
              color0: this.downColor,
              borderColor: this.upBorderColor,
              borderColor0: this.downBorderColor
            }
          },
          markPoint: this.hasOption ? {
            label: {
              normal: {
                formatter: (param) => {
                  return param != null ? Math.round(param.value) : '';
                }
              }
            },
            data: [

              {
                name: 'highest value',
                type: 'max',
                valueDim: 'highest'
              },
              {
                name: 'lowest value',
                type: 'min',
                valueDim: 'lowest'
              },
              {
                name: 'average value on close',
                type: 'average',
                valueDim: 'close'
              }
            ],
            tooltip: {
              formatter: (param) => {
                return param.name + '<br>' + (param.data.coord || '');
              }
            }
          } : {},
          markLine: {
            symbol: ['none', 'none'],
            data: this.hasOption ? [
              [{
                  name: 'from lowest to highest',
                  type: 'min',
                  valueDim: 'lowest',
                  symbol: 'circle',
                  symbolSize: 10,
                  label: {
                    normal: {
                      show: false
                    },
                    emphasis: {
                      show: false
                    }
                  }
                },
                {
                  type: 'max',
                  valueDim: 'highest',
                  symbol: 'circle',
                  symbolSize: 10,
                  label: {
                    normal: {
                      show: false
                    },
                    emphasis: {
                      show: false
                    }
                  }
                }
              ],
              {
                name: 'min line on close',
                type: 'min',
                valueDim: 'close'
              },
              {
                name: 'max line on close',
                type: 'max',
                valueDim: 'close'
              },

            ] : [],
          }
        },
        {
          name: 'MA5',
          type: 'line',
          data: this.calculateMA(5, data),
          smooth: true,
          lineStyle: {
            normal: {
              opacity: 0.5
            }
          }
        },
        {
          name: 'MA10',
          type: 'line',
          data: this.calculateMA(10, data),
          smooth: true,
          lineStyle: {
            normal: {
              opacity: 0.5
            }
          }
        },
        {
          name: 'MA20',
          type: 'line',
          data: this.calculateMA(20, data),
          smooth: true,
          lineStyle: {
            normal: {
              opacity: 0.5
            }
          }
        },
        {
          name: 'MA30',
          type: 'line',
          data: this.calculateMA(30, data),
          smooth: true,
          lineStyle: {
            normal: {
              opacity: 0.5
            }
          }
        },
        {
          name: 'MA30',
          type: 'line',
          data: this.calculateMA(30, data),
          smooth: true,
          lineStyle: {
            normal: {
              opacity: 0.5
            }
          }
        },
      ]
    };

    if (!this.hasOption) {
      option.series = option.series.filter(serieType => {
        return serieType.type == 'candlestick'
      });
    }


    option.series[0].markLine.data.push({
      type: 'line',
      yAxis: data.values[data.values.length - 1][1],
      lineStyle: {
        color: 'grey'
      },
      label: {
        formatter: (param) => {
          console.log(param)
          return '$ ' + param.value.toFixed(2);
        }
      }

    })



    var myChart = echarts.init(document.getElementById('main'));
    myChart.setOption(option, true);

  },

  createLineChart(type) {


    this.selectedChart = 'lineChart';

    let data = this.splitData([...this.financialsLine]);

    var optionLine = {
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'cross'
        }
      },
      xAxis: {
        type: 'category',
        data: data.categoryData,
        scale: true,
        boundaryGap: true,
        axisLine: {
          onZero: false
        },
        splitLine: {
          show: false
        },
      },
      yAxis: {
        scale: true,
        type: 'value',
          axisLabel: {
          formatter: (param) => {
            return '$ ' + param;
          }
        }
      },
      dataZoom: [{
          type: 'inside',
          start: this.period,
          end: 100
        },
        {
          show: true,
          type: 'slider',
          y: '90%',
          start: 50,
          end: 100
        }
      ],
      series: [{
        data: data.values,
        type: 'line',
        showSymbol: false,
        markLine:{
         symbol: ['none', 'none'],
         data: [],
        }
      }]
    };

    if (type && type == 'area') {
      optionLine.series[0].areaStyle = {};
      this.selectedChart = 'areaChart';

    }


  console.log(data.values[data.values.length - 1])
    optionLine.series[0].markLine.data.push({
      type: 'line',
      yAxis: data.values[data.values.length - 1],
      lineStyle: {
        color: 'grey'
      },
      label: {
        formatter: (param) => {
          console.log(param)
          return '$ ' + param.value.toFixed(2);
        }
      }

    })

    let myChart = echarts.init(document.getElementById('main'));
    myChart.setOption(optionLine, true);

  },

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  },

  splitData(financials) {
    let rawData = JSON.parse(JSON.stringify(financials));
    var categoryData = [];
    var values = []
    for (var i = 0; i < rawData.length; i++) {
      let dateFormated = this.formatDate(rawData[i].splice(0, 1)[0]);

      categoryData.push(dateFormated);

      if (rawData[i].length == 4) {

        let row = [rawData[i][1], rawData[i][2], rawData[i][0], rawData[i][3]]
        values.push(row)
      } else {
        let row = rawData[i][1]
        values.push(row)

      }

    }
    return {
      categoryData: categoryData,
      values: values
    };

  },

  calculateMA(dayCount, data) {
    var result = [];
    for (var i = 0, len = data.values.length; i < len; i++) {
      if (i < dayCount) {
        result.push('-');
        continue;
      }
      var sum = 0;
      for (var j = 0; j < dayCount; j++) {
        sum += data.values[i - j][1];
      }
      result.push(sum / dayCount);
    }
    return result;
  },

  setPeriod(period) {
    this.period = 100 - (period / this.financialsLine.length) * 100;
    if (this.selectedChart === 'candlestick')
      this.createCandlestickChart();
    else if (this.selectedChart === 'lineChart')
      this.createLineChart();
    else
      this.createLineChart('area');

  }
}

});

</script>
