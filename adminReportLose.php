<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BetStatus.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$tradeDetails = getBetstatus($conn," WHERE result_edited = 'WIN' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/adminReportLose.php" />
    <meta property="og:title" content="Admin Total Lose | De Xin Guo Ji 德鑫国际" />
    <title>Admin Total Lose | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/adminReportLose.php" />

    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAdmin.php'; ?>
    <?php include 'sidebar.php'; ?>
    <div class="width100 same-padding2">
    <h1 class="menu-distance h1-title white-text text-center">Lose Report</h1>
    <div class="width100 overflow blue-opa-bg padding-box radius-box">

    <div class="clear"></div>

        <div class="width100 shipping-div2">
            <div class="overflow-scroll-div">
                <table class="table-width data-table message-table">
                    <thead>
                        <tr>
                            <th>NO</th>
                            <th class="two-white-border">TIMELINE</th>
                            <th class="two-white-border">CURRENCY</th>
                            <th class="two-white-border">TRADE TYPE</th>
                            <th class="two-white-border">AMOUNT</th>
                            <th class="two-white-border">START RATE</th>
                            <th class="two-white-border">END RATE</th>
                            <th class="two-white-border">FINAL RESULT</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <?php
                        if($tradeDetails != null)
                        {
                        for($cnt = 0;$cnt < count($tradeDetails) ;$cnt++)
                        {?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $tradeDetails[$cnt]->getTimeline();?> sec</td>
                            <td><?php echo $tradeDetails[$cnt]->getCurrency();?></td>
                            <td><?php echo $tradeDetails[$cnt]->getBetType();?></td>
                            <td><?php echo $tradeDetails[$cnt]->getAmount();?></td>
                            <td><?php echo $tradeDetails[$cnt]->getStartRate();?></td>
                            <td><?php echo $tradeDetails[$cnt]->getEndRate();?></td>
                            <td><?php echo $tradeDetails[$cnt]->getResultEdited();?></td>
                        </tr>
                        <?php
                        }
                        }
                        ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>
    </div>
</div>

<?php include 'js.php'; ?>
</body>
</html>