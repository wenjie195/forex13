<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/BetStatus.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$messageValue = getMessage($conn," WHERE uid = ? AND reply_message != '' ",array("uid"),array($uid),"s");

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$time = $dt->format('Y-m-d H:i:s');
$playTime = $dt->format('s');


function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <meta property="og:url" content="https://dxforextrade88.com/tradeRecord.php" />
    <meta property="og:title" content="Trade | De Xin Guo Ji 德鑫国际" />
    <title>Trade | De Xin Guo Ji 德鑫国际</title>
    <link rel="canonical" href="https://dxforextrade88.com/tradeRecord.php" />
    <?php include 'css.php'; ?>
</head>
<body class="body">
<div class="dark-bg overflow same-padding">
	<?php include 'headerAfterLogin.php'; ?>

    <?php include 'userMessage.php'; ?>

    <div  class="width100 overflow blue-opa-bg border-div tab-div small-web-width menu-distance2">
    <div class="width100 overflow tab-divdiv tab">
    	<div class="w50-button">
        	<button class="tablinks clean" onclick="openTab(event, 'Win')"><?php echo _USERDASHBOARD_WIN ?></button>
        </div>
        <div class="w50-button">
        	<button class="tablinks clean" onclick="openTab(event, 'Loss')"><?php echo _USERDASHBOARD_LOSS ?></button>
        </div>
    </div>

    <div id="Win" class="tabcontent">
        <table class="table-width data-table">
            <thead>
                <tr>
                    <!-- <th class="first-width">
                        <form>
                            <input class="white-border-input search-input2" placeholder="<?php //echo _USERDASHBOARD_SEARCH ?>" type="text">
                            <button class="clean search-btn hover1">
                                <img src="img/search.png" class="search-img hover1a" alt="<?php //echo _USERDASHBOARD_SEARCH ?>" title="<?php //echo _USERDASHBOARD_SEARCH ?>">
                                <img src="img/search2.png" class="search-img hover1b" alt="<?php //echo _USERDASHBOARD_SEARCH ?>" title="<?php //echo _USERDASHBOARD_SEARCH ?>">
                            </button>
                        </form>
                    </th>
                    <th></th> -->
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><b><?php echo _USERDASHBOARD_INSTRUMENT ?></b></td>
                    <td><b><?php echo _USERDASHBOARD_AMOUNT ?></b></td>
                </tr>
                <?php
                $conn = connDB();
                $depositArray = getBetstatus($conn,"WHERE uid = ? AND result_edited = 'WIN' ORDER BY date_created DESC", array("uid") ,array($uid),"s");
                if($depositArray != null)
                {
                for($cnt = 0;$cnt < count($depositArray) ;$cnt++)
                {
                ?>
                <tr>
                <td><?php

                $currencyArrayEn = ("EUR/USD,USD/CAD,GBP/JPY,AUD/USD,USD/JPY,GBP/USD,USD/CHF,EUR/AUD,EUR/JPY,EUR/GBP,AUD/JPY,NZD/USD,EUR/CHF,GBP/CAD,CAD/JPY");
                $currencyArrayCh = ("欧元/美元,美元/加拿大元,英镑/日元,澳大利亚元/美元,美元/日元,英镑/美元,美元/瑞士法郎,欧元/澳大利亚元,欧元/日元,欧元/英镑,澳大利亚元/日元,新西兰元/美元,欧元/瑞士法郎,英镑/加拿大元,加拿大元/日元");
                $currencyArrayEnExp = explode(",",$currencyArrayEn);
                $currencyArrayChExp = explode(",",$currencyArrayCh);
                for ($i=0; $i <count($currencyArrayEnExp) ; $i++) {
                  if($currencyArrayEnExp[$i] == $depositArray[$cnt]->getCurrency()){
                    if ( isset($_SESSION['lang']) && $_SESSION['lang'] == 'en' || isset($_GET['lang']) && $_GET['lang'] == 'en') {
                      echo $currencyArrayEnExp[$i];
                    }else {
                      echo $currencyArrayChExp[$i];
                    }

                  }
                }?></td>
                <td class="green-text"><?php echo "$ ".number_format($depositArray[$cnt]->getAmount());?></td>
                <?php
                }
                ?>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>

    <div id="Loss" class="tabcontent">
        <table class="table-width data-table">
            <thead>
                <tr>
                    <!-- <th class="first-width">
                        <form>
                            <input class="white-border-input search-input2" placeholder="<?php //echo _USERDASHBOARD_SEARCH ?>" type="text">
                            <button class="clean search-btn hover1">
                                <img src="img/search.png" class="search-img hover1a" alt="<?php //echo _USERDASHBOARD_SEARCH ?>" title="<?php //echo _USERDASHBOARD_SEARCH ?>">
                                <img src="img/search2.png" class="search-img hover1b" alt="<?php //echo _USERDASHBOARD_SEARCH ?>" title="<?php //echo _USERDASHBOARD_SEARCH ?>">
                            </button>
                        </form>
                    </th>
                    <th></th> -->
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><b><?php echo _USERDASHBOARD_INSTRUMENT ?></b></td>
                    <td><b><?php echo _USERDASHBOARD_AMOUNT ?></b></td>
                </tr>
                <?php
                $conn = connDB();
                $depositArray = getBetstatus($conn,"WHERE uid = ? AND result_edited = 'LOSE' ORDER BY date_created DESC", array("uid") ,array($uid),"s");
                if($depositArray != null)
                {
                for($cnt = 0;$cnt < count($depositArray) ;$cnt++)
                {
                ?>
                <tr>
                <td><?php

                $currencyArrayEn = ("EUR/USD,USD/CAD,GBP/JPY,AUD/USD,USD/JPY,GBP/USD,USD/CHF,EUR/AUD,EUR/JPY,EUR/GBP,AUD/JPY,NZD/USD,EUR/CHF,GBP/CAD,CAD/JPY");
                $currencyArrayCh = ("欧元/美元,美元/加拿大元,英镑/日元,澳大利亚元/美元,美元/日元,英镑/美元,美元/瑞士法郎,欧元/澳大利亚元,欧元/日元,欧元/英镑,澳大利亚元/日元,新西兰元/美元,欧元/瑞士法郎,英镑/加拿大元,加拿大元/日元");
                $currencyArrayEnExp = explode(",",$currencyArrayEn);
                $currencyArrayChExp = explode(",",$currencyArrayCh);
                for ($i=0; $i <count($currencyArrayEnExp) ; $i++) {
                  if($currencyArrayEnExp[$i] == $depositArray[$cnt]->getCurrency()){
                    if ( isset($_SESSION['lang']) && $_SESSION['lang'] == 'en' || isset($_GET['lang']) && $_GET['lang'] == 'en') {
                      echo $currencyArrayEnExp[$i];
                    }else {
                      echo $currencyArrayChExp[$i];
                    }

                  }
                }?></td>
                <td class="red-text"><?php echo "$ ".number_format($depositArray[$cnt]->getAmount());?></td>
                <?php
                }
                ?>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>
    </div>

        <div id="buy-modal" class="modal-css">
            <!-- Modal content -->
            <div class="modal-content-css forgot-modal-content login-modal-content">
            <span id="close" class="close-css close-buy">&times;</span>
            <h1 id="getCurrenyName" class="h1-title white-text text-center"></h1>
                <!-- <form> -->
                <form action="utilities/submitTradeFunction.php" method="POST">
                    <div id="getCurrency" class="up-bottom-border">
                    <p class="input-top-text display-none"><?php echo _JS_TYPE ?></p>
                    <select class="clean  de-input display-none" id="trade_type" name="trade_type" required>
                        <option value="Buy" name="Buy"><?php echo _USERDASHBOARD_BUY ?></option>
                        <option value="Sell" name="Sell"><?php echo _USERDASHBOARD_SELL ?></option>
                    </select>

                    <p class="input-top-text">Timeline</p>
                    <div class="toggle">
                    <!-- <input type="radio" name="sizeBy" value="100" id="sizeWeight" class="clean" /> -->
                        <input type="radio" value="30" id="sizeDimensions" name="trade_timeline" class="clean"/>
                        <label for="sizeDimensions" class="clean w30">30</label>
                        <input type="radio" value="60" id="sizeDimensions1" name="trade_timeline" class="clean"/>
                        <label for="sizeDimensions1" class="clean mid-30 w30">60</label>
                        <input type="radio" value="180" id="sizeDimensions2" name="trade_timeline" class="clean"/>
                        <label for="sizeDimensions2" class="clean margin-right0 w30">180</label>
                    </div>
                    <!-- </div> -->
                    <div class="clear"></div>

                    <!-- <p class="input-top-text"><?php //echo _USERDASHBOARD_AMOUNT ?></p>
                    <button class="clean white-selection-btn" id="trade_amount" name="trade_amount" value="100">100</button>
                    <button class="clean white-selection-btn mid-30" id="trade_amount" name="trade_amount" value="300">300</button>
                    <button class="clean white-selection-btn" id="trade_amount" name="trade_amount" value="500">500</button>
                    <button class="clean white-selection-btn" id="trade_amount" name="trade_amount" value="1000">1000</button>
                    <button class="clean white-selection-btn mid-30" id="trade_amount" name="trade_amount" value="2000">2000</button>
                    <button class="clean white-selection-btn" id="trade_amount" name="trade_amount" value="3000">3000</button>
                    <button class="clean white-selection-btn width100-ow" id="trade_amount" name="trade_amount" value="5000">5000</button> -->

                    <p class="input-title-p"><?php echo _USERDASHBOARD_AMOUNT ?></p>
                        <div class="toggle">
                            <input type="radio" value="100" id="trade_amount1" name="trade_amount" class="clean white-selection-btn"/>
                            <label for="trade_amount1" class="clean w30">100</label>
                            <input type="radio" value="300" id="trade_amount2" name="trade_amount" class="clean white-selection-btn"/>
                            <label for="trade_amount2" class="clean mid-30 w30">300</label>
                            <input type="radio" value="500" id="trade_amount3" name="trade_amount" class="clean white-selection-btn"/>
                            <label for="trade_amount3" class="clean w30">500</label>
                            <input type="radio" value="1000" id="trade_amount4" name="trade_amount" class="clean white-selection-btn"/>
                            <label for="trade_amount4" class="clean w30">1000</label>
                            <input type="radio" value="2000" id="trade_amount5" name="trade_amount" class="clean white-selection-btn"/>
                            <label for="trade_amount5" class="clean mid-30 w30">2000</label>
                            <input type="radio" value="3000" id="trade_amount6" name="trade_amount" class="clean white-selection-btn"/>
                            <label for="trade_amount6" class="clean w30">3000</label>
                            <input type="radio" value="5000" id="trade_amount7" name="trade_amount" class="clean white-selection-btn width100-ow"/>
                            <label for="trade_amount7" class="clean width100-ow">5000</label>
                        </div>

                    <input class="clean de-input" type="hidden" name="date" value="<?php echo date('Y-m-d H:i:s'); ?>" readonly>
                    <input class="clean de-input" type="hidden" id="user_uid" name="user_uid" value="<?php echo $userDetails->getUid();?>" readonly>
                    <input class="clean de-input" type="hidden" id="user_credit" name="user_credit" value="<?php echo $userDetails->getCredit();?>" readonly>
                    <input class="clean de-input" type="hidden" id="timer" name="timer" value="<?php echo $playTime ?>" readonly>

                    <div class="clear"></div>

                    <p class="input-top-text">Other Amount</p>
                    <input class="clean de-input" type="text" placeholder="Trade Amount" id="trade_other_amount" name="trade_other_amount">

                    <button class="clean blue-button width100 small-distance small-distance-bottom" name="submit_trade">Submit</button>

                    </div>
                </form>
            </div>
        </div>

      <div id="withdraw-modal" class="modal-css">
          <!-- Modal content -->
          <div class="modal-content-css forgot-modal-content login-modal-content">
              <span class="close-css close-withdraw">&times;</span>
              <h1 class="h1-title white-text text-center"><?php echo _USERDASHBOARD_WITHDRAW ?></h1>
              <!-- <form> -->
              <form action="utilities/submitWithdrawalFunction.php" method="POST">
              <div class="up-bottom-border">
                  <p class="input-top-text"><?php echo _USERDASHBOARD_BALANCE ?></p>
                  <!-- <p class="clean de-input no-input-style">$1,000,000</p> -->
                  <p class="clean de-input no-input-style">$<?php echo $credit;?></p>
                  <p class="input-top-text">Bank Name</p>
                  <input class="clean de-input" type="text" placeholder="Bank Name" id="withdrawal_bank_name" name="withdrawal_bank_name" required>
                  <p class="input-top-text">Bank Account Holder Name</p>
                  <input class="clean de-input" type="text" placeholder="Bank Account Holder Name" id="withdrawal_bank_acc_holder" name="withdrawal_bank_acc_holder" required>
                  <p class="input-top-text">Bank Account Number</p>
                  <input class="clean de-input" type="text" placeholder="Bank Account Number" id="withdrawal_bank_acc_number" name="withdrawal_bank_acc_number" required>
                  <p class="input-top-text"><?php echo _JS_WITHDRAW_AMOUNT ?></p>
                  <!-- <input class="clean de-input" type="number" placeholder="<?php echo _USERDASHBOARD_AMOUNT ?>" required> -->
                  <input class="clean de-input" type="text" placeholder="<?php echo _USERDASHBOARD_AMOUNT ?>"  id="withdrawal_amount" name="withdrawal_amount" required>
              </div>

              <input type="hidden" id="withdrawal_uid" name="withdrawal_uid" value="<?php echo $userDetails->getUid();?>" readonly>
              <input type="hidden" id="withdrawal_currentcredit" name="withdrawal_currentcredit" value="<?php echo $userDetails->getCredit();?>" readonly>

              <div class="clear"></div>
                  <button class="clean blue-button width100 small-distance small-distance-bottom"><?php echo _USERDASHBOARD_WITHDRAW ?></button>
              <div class="clear"></div>
              </form>
          </div>
      </div>

      <!-- withdraw Modal -->
      <div id="customerservice-modal" class="modal-css">
          <!-- Modal content -->
          <div class="modal-content-css forgot-modal-content login-modal-content">
              <span class="close-css close-customerservice">&times;</span>
              <h1 class="h1-title white-text text-center">Customer Service</h1>
              <!-- <form action="utilities/submitWithdrawalFunction.php" method="POST"> -->
              <form action="utilities/submitCSFunction.php" method="POST">
              <div class="up-bottom-border">

                  <p class="input-top-text">Message</p>
                  <input class="clean de-input" type="text" placeholder="Your Message" id="message_details" name="message_details" required>

              </div>

              <input type="hidden" id="sender_uid" name="sender_uid" value="<?php echo $userDetails->getUid();?>" readonly>

              <div class="clear"></div>
                  <button class="clean blue-button width100 small-distance small-distance-bottom">Click To Sent</button>
              <div class="clear"></div>
              </form>
          </div>
      </div>
    </div>
</div>

<?php include 'js.php'; ?>

</body>

<script type="text/javascript">
$(document).ready(function(){
 $("#Transaction").load("userTabs.php");
    setInterval(function() {
        $("#Transaction").load("userTabs.php");
    }, 1000);
    $("#Position").load("userPosition.php");
       setInterval(function() {
           $("#Position").load("userPosition.php");
       }, 1000);
    $("#currency_name").click( function(){
      var currenyName = $(this).val();
      $("#getCurrenyName").text(currenyName);
    });

    $("#Win").show();
});
</script>
</html>
